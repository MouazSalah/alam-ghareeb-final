package grand.app.alamghareeb.auth.login.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.google.firebase.messaging.FirebaseMessaging
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.BaseAuthFragment
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.auth.login.model.LoginResponse
import grand.app.alamghareeb.auth.login.viewmodel.LoginViewModel
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentLoginBinding
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class LoginFragment : BaseAuthFragment() {
    lateinit var binding: FragmentLoginBinding

    @Inject
    lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            when {
                !task.isSuccessful -> {
                    Timber.e(task.exception.toString())
                    return@addOnCompleteListener
                }
                task.result != null -> {
                    viewModel.request.device_token = task.result
                }
            }
        }

        binding.ibBack.setOnClickListener {
            requireActivity().finishAffinity()
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is LoginResponse -> {
                            when (it.data.code) {
                                405 -> {
                                    findNavController().navigate(LoginFragmentDirections.loginToVerify(
                                        viewModel.request.phone!!,
                                        Codes.REGISTER_INTENT))
                                }
                                else -> {
                                    when (Const.isAskedToLogin) {
                                        0 -> {
                                            requireActivity().startActivity(Intent(requireActivity(),
                                                MainActivity::class.java))
                                            requireActivity().finish()
                                        }
                                        1 -> {
                                            requireActivity().finish()
                                        }
                                    }
                                }
                            }
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.REGISTER_INTENT -> {
                                    findNavController().navigate(R.id.login_to_register)
                                }
                                Codes.FORGOT_INTENT -> {
                                    findNavController().navigate(R.id.login_to_forgot_password)
                                }
                                Codes.SKIP_CLICKED -> {
                                    requireActivity().startActivity(Intent(requireActivity(),
                                        MainActivity::class.java))
                                    requireActivity().finishAffinity()
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}