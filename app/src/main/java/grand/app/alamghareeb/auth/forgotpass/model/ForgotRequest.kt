package grand.app.alamghareeb.auth.forgotpass.model

data class ForgotRequest(
    var phone: String? = null,
    var type: String? = "verify"
)
