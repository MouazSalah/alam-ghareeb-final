package grand.app.alamghareeb.auth.resetpass.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.auth.resetpass.model.ResetRequest
import grand.app.alamghareeb.auth.resetpass.model.ResetPassResponse
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class ResetPassViewModel @Inject constructor() : BaseViewModel() {
    var request = ResetRequest()

    fun onChangeClicked() {
        setClickable()
        when {
            request.password.isNullOrEmpty() || request.password.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_password))
            }
            request.password!!.length < 6 -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_invalid_password))
            }
            request.password_confirmation.isNullOrEmpty() || request.password_confirmation.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_confirm_password))
            }
            request.password_confirmation != request.password -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_not_match))
            }
            else -> {
                resetPassword()
            }
        }
    }

    private fun resetPassword() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().requestPostBody(URLS.CHANGE_PASSWORD,
                request)
        }) { res ->
            obsIsProgress.set(false)
            val response: ResetPassResponse = Gson().fromJson(res, ResetPassResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.msg)
                }
            }
        }
    }
}