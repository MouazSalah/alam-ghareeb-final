package grand.app.alamghareeb.auth.verifycode.viewmodel

import android.os.CountDownTimer
import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.auth.forgotpass.model.ForgotPassResponse
import grand.app.alamghareeb.auth.verifycode.model.VerifyRequest
import grand.app.alamghareeb.auth.verifycode.model.VerifyResponse
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class VerifyViewModel @Inject constructor() : BaseViewModel() {
    var request = VerifyRequest()
    var obsTimer = ObservableField<String>()
    var obsClickable = ObservableField(false)
    lateinit var timer: CountDownTimer

    fun onSendClicked() {
        setClickable()
        when {
            request.code.isNullOrEmpty() || request.code.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_code))
            }
            request.code!!.length < 4 -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_short_code))
            }
            else -> {
                verifyCode()
            }
        }
    }

    fun onResendClicked() {
        setClickable()
        resendCode()
        timer.cancel()
        timer.start()
    }

    private fun startTimer() {
        timer = object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                obsTimer.set("${"00:"}${millisUntilFinished / 1000}")
                obsClickable.set(false)
            }

            override fun onFinish() {
                obsTimer.set(BaseApp.getInstance.getString(R.string.label_time_zero))
                obsClickable.set(true)
            }
        }.start()
    }

    private fun verifyCode() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().requestPostBody(URLS.VERIFY_CODE,
                request)
        }) { res ->
            obsIsProgress.set(false)
            val response: VerifyResponse = Gson().fromJson(res, VerifyResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    private fun resendCode() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().requestPostBody(URLS.FORGOT_PASSWORD,
                request)
        }) { res ->
            obsIsProgress.set(false)
            val response: ForgotPassResponse = Gson().fromJson(res, ForgotPassResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    init {
        startTimer()
    }
}