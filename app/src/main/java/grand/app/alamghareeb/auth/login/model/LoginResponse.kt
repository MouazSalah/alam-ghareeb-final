package grand.app.alamghareeb.auth.login.model

import com.google.gson.annotations.SerializedName

data class LoginResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val userData: UserData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class UserData(

    @field:SerializedName("image")
    val image: Any? = null,

    @field:SerializedName("wallet")
    val wallet: Int? = null,

    @field:SerializedName("phone")
    val phone: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("notification_count")
    val notificationCount: Int? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("token")
    val jwt: String? = null
)
