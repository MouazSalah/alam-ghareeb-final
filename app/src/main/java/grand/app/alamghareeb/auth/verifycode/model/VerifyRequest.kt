package grand.app.alamghareeb.auth.verifycode.model

data class VerifyRequest(
    var phone: String? = null,
    var type: String? = "verify",
    var code: String? = null
)