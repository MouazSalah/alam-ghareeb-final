package grand.app.alamghareeb.auth.login.model

data class LoginRequest(
    var phone: String? = null,
    var password: String? = null,
    var device_token: String? = null
)