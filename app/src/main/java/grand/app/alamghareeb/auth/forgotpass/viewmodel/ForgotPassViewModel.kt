package grand.app.alamghareeb.auth.forgotpass.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.auth.forgotpass.model.ForgotPassResponse
import grand.app.alamghareeb.auth.forgotpass.model.ForgotRequest
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class ForgotPassViewModel @Inject constructor() : BaseViewModel() {
    var request = ForgotRequest()

    fun onSendClicked() {
        setClickable()
        when {
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_phone))
            }
            else -> {
                sendCodeToVerify()
            }
        }
    }

    private fun sendCodeToVerify() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().requestPostBody(URLS.FORGOT_PASSWORD,
                request)
        }) { res ->
            obsIsProgress.set(false)
            val response: ForgotPassResponse = Gson().fromJson(res, ForgotPassResponse::class.java)
            when (response.code) {
                200, 405 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }
}