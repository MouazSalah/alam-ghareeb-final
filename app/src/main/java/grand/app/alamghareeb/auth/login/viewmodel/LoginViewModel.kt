package grand.app.alamghareeb.auth.login.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.auth.login.model.LoginRequest
import grand.app.alamghareeb.auth.login.model.LoginResponse
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.getFireBaseToken
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class LoginViewModel @Inject constructor() : BaseViewModel() {
    var request = LoginRequest()

    fun onLoginClicked() {
        setClickable()
        when {
            request.phone.isNullOrEmpty() || request.phone.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_login_key))
            }
            request.password.isNullOrEmpty() || request.password.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_password))
            }
            request.password!!.length < 6 -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_invalid_password))
            }
            else -> {
                login()
            }
        }
    }

    fun login() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.LOGIN, request) }) { res ->
            obsIsProgress.set(false)
            val response: LoginResponse = Gson().fromJson(res, LoginResponse::class.java)
            when (response.code) {
                200, 405 -> {
                    PrefMethods.saveUserData(response.userData)
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onButtonClicked(value: Int) {
        setClickable()
        apiResponseLiveData.value = ApiResponse.success(value)
    }
}