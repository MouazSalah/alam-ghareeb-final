package grand.app.alamghareeb.base

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ProgressBar
import androidx.activity.addCallback
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.google.android.material.bottomnavigation.BottomNavigationView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.dialogs.toast.DialogToastFragment
import grand.app.alamghareeb.location.util.PermissionUtil
import grand.app.alamghareeb.utils.Constants
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

open class BaseFragment : Fragment() {
    var isNavigated = false

    fun navigateWithAction(action: NavDirections) {
        isNavigated = true
        findNavController().navigate(action)
    }

    fun navigate(resId: Int) {
        isNavigated = true
        findNavController().navigate(resId)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (!isNavigated)
            requireActivity().onBackPressedDispatcher.addCallback(this) {
                val navController = findNavController()
                when {
                    navController.currentBackStackEntry?.destination?.id != null -> {
                        findNavController().navigateUp()
                    }
                    else -> navController.popBackStack()
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    fun showToast(msg: String, type: Int) {
        val bundle = Bundle()
        bundle.putString(Params.DIALOG_TOAST_MESSAGE, msg)
        bundle.putInt(Params.DIALOG_TOAST_TYPE, type)
        Utils.startDialogActivity(requireActivity(),
            DialogToastFragment::class.java.name,
            Codes.DIALOG_TOAST_REQUEST,
            bundle)
    }

//    fun showBottomBar(show: Boolean)
//    {
//        try
//        {
//            val navView: BottomNavigationView = requireActivity().findViewById(R.id.bottomBar)
//            when {
//                show -> {
//                    navView.visibility = View.VISIBLE
//                }
//                else -> navView.visibility = View.GONE
//            }
//        }
//        catch (e: Exception)
//        {
//            Timber.e(e)
//        }
//    }

    protected fun pickImageDialogSelect() {
        // FileOperations.pickImage(BaseApp.getInstance, this@BaseFragment, Constants.FILE_TYPE_IMAGE)
    }

    protected fun pickDocument() {
//        FileOperations.pickDocument(
//            if (context != null) context else context2,
//            this@BaseFragment,
//            Constants.FILE_TYPE_DOCUMENT
//        )
    }

    var dialogLoader: Dialog? = null
    private fun initializeProgress() {
        //TODO loader animation
        val view: View =
            LayoutInflater.from(BaseApp.getInstance).inflate(R.layout.loader_animation, null)
        val builder = AlertDialog.Builder(BaseApp.getInstance, R.style.customDialog)
        builder.setView(view)
        dialogLoader = builder.create()
        // dialogLoader.setCancelable(false);
        dialogLoader?.setOnCancelListener(DialogInterface.OnCancelListener {
            dialogLoader?.dismiss()
            requireActivity().onBackPressed()
            // requireActivity().finish()
        })
    }

    private fun showProgress() {
        initializeProgress()
        //show dialog
        if (dialogLoader != null) {
            // Log.d(TAG, "isFinishing , dialogLoader")
            dialogLoader!!.show()
        } else {
            showProgress()
            // Log.d(TAG, "isFinishing $TAG")
        }
    }

    private fun hideProgress() {
        if (dialogLoader != null && dialogLoader!!.isShowing) dialogLoader!!.dismiss()
    }


    fun handleActions(mutable: Mutable) {
        if (mutable.message == Constants.SHOW_PROGRESS) showProgress()
        else if (mutable.message == Constants.HIDE_PROGRESS) hideProgress()
        else if (mutable.message == Constants.SERVER_ERROR && mutable.obj == null) {
            hideProgress()
        } else if (mutable.message == Constants.ERROR && mutable.obj is String) {
            hideProgress()
        } else if (mutable.message == Constants.FAILURE_CONNECTION) {
            hideProgress()
        }

        //getActivityBase().handleActions(mutable)
    }

    fun pickImage(requestCode: Int) {
        val options = Options.init()
            .setRequestCode(requestCode) //Request code for activity results
            .setFrontfacing(false) //Front Facing camera on start
            .setExcludeVideos(true) //Option to exclude videos
        when {
            PermissionUtil.hasImagePermission(requireActivity()) -> {
                Pix.start(this, options)
            }
            else -> {
                observe(
                    PermissionUtil.requestPermission(
                        requireActivity(),
                        PermissionUtil.getImagePermissions()
                    )
                ) {
                    when (it) {
                        PermissionUtil.AppPermissionResult.AllGood -> Pix.start(
                            this,
                            options
                        )
                    }
                }
            }
        }
    }
}

