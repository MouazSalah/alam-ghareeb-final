package grand.app.alamghareeb.base.di

import dagger.Component
import grand.app.alamghareeb.activity.BaseActivity
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.auth.conditions.ConditionsActivity
import grand.app.alamghareeb.auth.forgotpass.view.ForgotPassFragment
import grand.app.alamghareeb.auth.login.view.LoginFragment
import grand.app.alamghareeb.auth.register.view.RegisterFragment
import grand.app.alamghareeb.auth.resetpass.view.ResetPassFragment
import grand.app.alamghareeb.auth.splash.SplashFragment
import grand.app.alamghareeb.auth.verifycode.view.VerifyFragment
import grand.app.alamghareeb.main.aboutapp.view.AboutFragment
import grand.app.alamghareeb.main.addaddress.view.AddAddressActivity
import grand.app.alamghareeb.main.addresses.view.AddressesListFragment
import grand.app.alamghareeb.main.cart.view.CartFragment
import grand.app.alamghareeb.main.categories.view.CategoriesFragment
import grand.app.alamghareeb.network.ConnectionModule
import grand.app.alamghareeb.main.changepassword.view.ChangePassFragment
import grand.app.alamghareeb.main.contactus.view.ContactUsFragment
import grand.app.alamghareeb.main.favorites.view.FavoritesFragment
import grand.app.alamghareeb.main.groups.base.GroupsFragment
import grand.app.alamghareeb.main.groups.mostselling.view.MostSellingFragment
import grand.app.alamghareeb.main.groups.oldorders.view.OldOrdersFragment
import grand.app.alamghareeb.main.groups.savingoffers.view.SavingOffersFragment
import grand.app.alamghareeb.main.home.view.HomeFragment
import grand.app.alamghareeb.main.offers.view.OffersFragment
import grand.app.alamghareeb.main.orders.view.OrdersFragment
import grand.app.alamghareeb.main.productdetails.view.ProductDetailsFragment
import grand.app.alamghareeb.main.products.view.ProductsFragment
import grand.app.alamghareeb.main.profile.view.ProfileFragment
import grand.app.alamghareeb.main.suggestion.view.SuggestFragment
import grand.app.alamghareeb.main.toprated.view.TopRatedFragment
import grand.app.alamghareeb.main.wallet.view.WalletFragment
import javax.inject.Singleton

//Component refer to an interface or waiter for make an coffee cup to me
@Singleton
@Component(modules = [ConnectionModule::class, grand.app.alamghareeb.base.LiveData::class])
interface IApplicationComponent {
    fun inject(tmpActivity: BaseActivity)
    fun inject(authActivity: AuthActivity)
    fun inject(mainActivity: MainActivity)
    fun inject(addAddressActivity: AddAddressActivity)
    fun inject(conditionsActivity: ConditionsActivity)

    fun inject(splashFragment: SplashFragment)
    fun inject(loginFragment: LoginFragment)
    fun inject(registerFragment: RegisterFragment)
    fun inject(forgotPassword: ForgotPassFragment)
    fun inject(verifyCodeFragment: VerifyFragment)
    fun inject(changePasswordFragment: ChangePassFragment)
    fun inject(resetPasswordFragment: ResetPassFragment)

    fun inject(homeFrag: HomeFragment)
    fun inject(categoriesFrag: CategoriesFragment)
    fun inject(offersFrag: OffersFragment)
    fun inject(profileFrag: ProfileFragment)
    fun inject(cartFrag: CartFragment)
    fun inject(topRatedFrag: TopRatedFragment)
    fun inject(productsFrag: ProductsFragment)

    fun inject(suggestFrag: SuggestFragment)
    fun inject(contactFrag: ContactUsFragment)
    fun inject(aboutFragment: AboutFragment)
    fun inject(productDetailsFragment: ProductDetailsFragment)

    fun inject(groupsFrag: GroupsFragment)
    fun inject(savingOffersFrag: SavingOffersFragment)
    fun inject(oldOrdersFrag: OldOrdersFragment)
    fun inject(mostSellingFrag: MostSellingFragment)
    fun inject(myOrdersFrag: OrdersFragment)
    fun inject(favoritesFrag: FavoritesFragment)
    fun inject(walletFrag: WalletFragment)

    fun inject(addressesFrag: AddressesListFragment)

    @Component.Builder
    interface Builder {
        open fun build(): IApplicationComponent?
    }
}