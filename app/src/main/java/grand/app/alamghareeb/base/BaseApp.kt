package grand.app.alamghareeb.base

import android.app.Application
import android.content.Context
import androidx.databinding.DataBindingUtil
import androidx.multidex.MultiDex
import grand.app.alamghareeb.base.di.DaggerIApplicationComponent
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.AppDataBindingComponent
import grand.app.alamghareeb.utils.LocalUtil
import timber.log.Timber

class BaseApp : Application() {
    var applicationComponent: IApplicationComponent? = null

    override fun onCreate() {
        super.onCreate()
        getInstance = this
        initTimber()
        DataBindingUtil.setDefaultComponent(AppDataBindingComponent())

        applicationComponent = DaggerIApplicationComponent.builder().build()
    }

    private fun initTimber() {
        Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return super.createStackElementTag(element) + " line: " + element.lineNumber
            }
        })
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocalUtil.onAttach(base))
        MultiDex.install(this)
    }

    companion object {
        lateinit var getInstance: BaseApp
    }
}