package grand.app.alamghareeb.databinding

import android.graphics.Bitmap
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.bumptech.glide.Glide
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.utils.isValidUrl
import grand.app.alamghareeb.utils.loadImageFromURL
import grand.app.alamghareeb.utils.resources.ResourceManager
import grand.app.alamghareeb.utils.resources.ResourceManager.getString
import timber.log.Timber

/**
 * Created by MouazSalah on 15/04/2021.
 **/
class OtherViewsBinding {

    @BindingAdapter("setImage")
    fun setImage(imageView: ImageView, obj: Any?) {
        obj?.let {
            when (it) {
                is Int -> imageView.setImageResource(it)
                is Drawable -> imageView.setImageDrawable(it)
                is Bitmap -> imageView.setImageBitmap(it)
                is Uri -> imageView.setImageURI(it)
                is String ->
                    when {
                        it.isValidUrl() -> imageView.loadImageFromURL(it)
                        else -> {
                            Timber.e("image url isn't valid")
                            // imageView.loadImageFromURL("")
                            imageView.setImageResource(R.drawable.ic_logo)
                        }
                    }
            }
        } ?: imageView.setImageResource(R.drawable.ic_logo)
    }

    @BindingAdapter("strokeTextView")
    fun strokeTextView(view: TextView, value: Double?) {
        when (value) {
            0.0 -> {
                view.visibility = View.GONE
            }
            null -> {
                view.visibility = View.GONE
            }
            else -> {
                view.visibility = View.VISIBLE
                view.paintFlags = view.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                view.text = "$value ${ResourceManager.getString(R.string.label_currency)}"
            }
        }
    }

    @BindingAdapter("registerState")
    fun registerState(view: ImageView, isEntered: Boolean?) {
        when (isEntered == false) {
            true -> {
                Glide.with(view.context).load(R.drawable.ic_branch_checked).into(view)
            }
            else -> {
                Glide.with(view.context).load(R.drawable.ic_branch_checked).into(view)
            }
        }
    }

    @BindingAdapter("registerLineState")
    fun registerLineState(view: ImageView, isEntered: Boolean?) {
        when (isEntered == false) {
            true -> {
                Glide.with(view.context).load(R.drawable.ic_branch_checked).into(view)
            }
            else -> {
                Glide.with(view.context).load(R.drawable.ic_branch_checked).into(view)
            }
        }
    }

    @BindingAdapter("notifyCount")
    fun notifyCount(view: TextView, value: Int?) {
        when (value) {
            null -> {
                view.visibility = View.GONE
            }
            0 -> {
                view.visibility = View.GONE
            }
            else -> {
                view.visibility = View.VISIBLE
                view.text = value.toString()
            }
        }
    }

    @BindingAdapter("isCreditZero")
    fun isCreditZero(view: TextView, value: Double?) {
        when (value) {
            null -> {
                view.text = "${getString(R.string.label_no_restored_products)}"
            }
            0.0 -> {
                view.text = "${getString(R.string.label_no_restored_products)}"
            }
            else -> {
                view.text =
                    "${getString(R.string.label_products_restored_by)}  $value ${getString(R.string.label_currency)}"
            }
        }
    }

    @BindingAdapter("isListEmpty")
    fun isListEmpty(view: View, value: Int?) {
        when (value) {
            null -> {
                view.visibility = View.GONE
            }
            else -> {
                view.visibility = View.VISIBLE
            }
        }
    }

    @BindingAdapter("isEmpty")
    fun isEmpty(view: View, value: String?) {
        when (value) {
            null -> {
                view.visibility = View.GONE
            }
            else -> {
                view.visibility = View.VISIBLE
            }
        }
    }

//    @BindingAdapter("setBackgroundColor")
//    fun setBackgroundColor(view: View, value: String?) {
//        Timber.e("mou3aaaz_color : " + value)
//        when {
//            value != null -> {
//                val color : Int = Color.parseColor(value)
//               view.setBackgroundColor(color)
//            }
//        }
//    }

    @BindingAdapter("isFavorite")
    fun isFavorite(view: ImageView, value: Boolean) {
        when (value) {
            true -> {
                view.setImageResource(R.drawable.ic_fav)
            }
            else -> {
                view.setImageResource(R.drawable.ic_un_fav)
            }
        }
    }

    @BindingAdapter("isNew")
    fun isNew(view: View, value: Int) {
        when (value) {
            1 -> {
                view.visibility = View.VISIBLE
            }
            else -> {
                view.visibility = View.GONE
            }
        }
    }

    @BindingAdapter("refreshing")
    fun setRefreshing(view: SwipeRefreshLayout, refreshing: Boolean) {
        if (refreshing != view.isRefreshing) {
            view.isRefreshing = refreshing
        }
    }

    /* Showing Shimmer Layouts while requesting API calls */
    @BindingAdapter("showShimmer")
    fun showShimmer(view: View, state: LoadingStatus?) {
        when (state) {
            LoadingStatus.SHIMMER -> {
                view.visibility = View.VISIBLE
            }
            else -> {
                view.visibility = View.GONE
            }
        }
    }

    /* Showing Shimmer Layouts while requesting API calls */
    @BindingAdapter("showFull")
    fun showFull(view: View, state: LoadingStatus?) {
        when (state) {
            LoadingStatus.FULL -> {
                view.visibility = View.VISIBLE
            }
            else -> {
                view.visibility = View.GONE
            }
        }
    }

    /* Showing Shimmer Layouts while requesting API calls */
    @BindingAdapter("showEmpty")
    fun showEmpty(view: View, state: LoadingStatus?) {
        when (state) {
            LoadingStatus.EMPTY -> {
                view.visibility = View.VISIBLE
            }
            else -> {
                view.visibility = View.GONE
            }
        }
    }

    /* Showing Shimmer Layouts while requesting API calls */
    @BindingAdapter("showNotLogin")
    fun showNotLogin(view: View, state: LoadingStatus?) {
        when (state) {
            LoadingStatus.NOTLOGIN -> {
                view.visibility = View.VISIBLE
            }
            else -> {
                view.visibility = View.GONE
            }
        }
    }

    @BindingAdapter("isHome")
    fun isHome(view: ImageView, flag: Int?) {
        when (flag) {
            1, 3 -> {
                view.visibility = View.VISIBLE
            }
            else -> {
                view.visibility = View.GONE
            }
        }
    }

    @BindingAdapter("isSalon")
    fun isSalon(view: ImageView, flag: Int?) {
        when (flag) {
            2, 3 -> {
                view.visibility = View.VISIBLE
            }
            else -> {
                view.visibility = View.GONE
            }
        }
    }

    @BindingAdapter("isVisible")
    fun isVisible(view: View, value: Boolean?) {
        if (value == false) {
            view.visibility = View.GONE
        } else {
            view.visibility = View.VISIBLE
        }
    }

    /* Which shows the address on MAP activity ... Urgent */
    @BindingAdapter("showAddressLayout")
    fun showAddressLayout(view: ConstraintLayout, address: String?) {
        if (address == null) {
            view.visibility = View.GONE
        } else {
            view.visibility = View.VISIBLE
        }
    }

    @BindingAdapter("showSubCategories")
    fun showSubCategories(view: View, state: Int?) {
        if (state == -1) {
            view.visibility = View.GONE
        } else {
            view.visibility = View.VISIBLE
        }
    }

    @BindingAdapter("visibleGone")
    fun visibleGone(view: View, state: Boolean?) {
        if (state == true) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }

    @BindingAdapter("showCancelMark")
    fun showCancelMark(view: View, state: Boolean?) {
        if (state == true) {
            Timber.e("mou3aaaz 111 : " + state)
            view.visibility = View.VISIBLE
        } else {
            Timber.e("mou3aaaz 222 : " + state)
            view.visibility = View.GONE
        }
    }

    @BindingAdapter("hidePrice")
    fun hidePrice(view: View, state: Boolean?) {
        if (state == true) {
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.INVISIBLE
        }
    }

    @BindingAdapter("showPass")
    fun showPass(view: EditText, isShown: Boolean?) {
        if (isShown == true) {
            view.transformationMethod = HideReturnsTransformationMethod.getInstance()
        } else {
            view.transformationMethod = PasswordTransformationMethod.getInstance()
        }
    }


//
//    @BindingAdapter("renderHtml")
//    fun bindRenderHtml(view: TextView, description: String?) {
//        if (description != null) {
//            view.text = HtmlCompat.fromHtml(description, HtmlCompat.FROM_HTML_MODE_COMPACT)
//            view.movementMethod = LinkMovementMethod.getInstance()
//        } else {
//            view.text = ""
//        }
//    }
//
//    @BindingAdapter("app:icon")
//    fun bindIconButton(btn: MaterialButton, resId: Int) {
//        resId.let {
//            btn.icon = ContextCompat.getDrawable(btn.context, resId)
//        }
//    }
//
//    @BindingAdapter("app:animateSplashImage")
//    fun bindSplashImage(imageView: ImageView, b: Boolean) {
//        val padding = 90
//        if (b) {
//            val params = imageView.layoutParams as ConstraintLayout.LayoutParams
//            params.verticalBias = 0.2f // here is one modification for example.
//            imageView.layoutParams = params
//            imageView.setPadding(padding, padding, padding, padding)
//        }
//    }
//    @BindingAdapter("app:lockView")
//    fun bindLockView(view: View, b: Boolean) {
//        view.isEnabled = !b
//    }
}