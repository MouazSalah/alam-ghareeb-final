package grand.app.alamghareeb.activity.auth

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseFragment

open class BaseAuthFragment : BaseFragment() {
    override fun onDestroyView() {
        super.onDestroyView()
        when {
            !isNavigated -> requireActivity().onBackPressedDispatcher.addCallback(this) {
                val navController = findNavController()
                when {
                    navController.currentBackStackEntry?.destination?.id != null -> {
                        findNavController().navigateUp()
                    }
                    else -> navController.popBackStack()
                }
            }
        }
    }

    fun showProgressBar(show: Boolean) {
        val navView: ProgressBar = requireActivity().findViewById(R.id.auth_progress_bar)
        when {
            show -> {
                navView.visibility = View.VISIBLE
            }
            else -> navView.visibility = View.GONE
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

//    fun setBarName(title: String, show : Boolean = false , color: Int = ContextCompat.getColor(requireActivity(), R.color.white)) {
//        (requireActivity() as AuthActivity).viewModel?.obsShowToolbar!!.set(show)
//        (requireActivity() as AuthActivity).customToolBar(title, show , color)
//    }

}

