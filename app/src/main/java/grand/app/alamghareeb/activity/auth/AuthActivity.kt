package grand.app.alamghareeb.activity.auth

import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.BaseActivity
import grand.app.alamghareeb.databinding.ActivityAuthBinding
import timber.log.Timber

class AuthActivity : BaseActivity() {
    private lateinit var binding: ActivityAuthBinding
    lateinit var viewModel: AuthViewModel
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth)
        viewModel = ViewModelProvider(this@AuthActivity).get(AuthViewModel::class.java)
        binding.viewModel = viewModel

        navController = findNavController(R.id.nav_home_host_fragment)

        binding.ibBack.setOnClickListener {
            when (navController.currentDestination?.id) {
                R.id.navigation_login -> {
                    finishAffinity()
                }
                else -> {
                    navController.navigateUp()
                }
            }
        }
        setUpToolbarAndStatusBar()
    }

    private fun setUpToolbarAndStatusBar() {
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.navigation_splash -> {
                    customToolBar(showToolBar = false)
                }
                R.id.navigation_login -> {
                    customToolBar(showToolBar = true, showBackBtn = false)
                }
                R.id.navigation_register, R.id.navigation_forgot_password,
                R.id.navigation_verify_code, R.id.navigation_reset_password
                -> {
                    customToolBar(showToolBar = true, showBackBtn = true)
                }
                else -> {
                    customToolBar(showToolBar = true, showBackBtn = true)
                }
            }
        }
    }

    private fun customToolBar(
        title: String = "",
        showToolBar: Boolean = false,
        showSkip: Boolean = false,
        showBackBtn: Boolean = false,
        toolBarColor: Int = ContextCompat.getColor(this, R.color.color_primary)
    ) {

        viewModel.obsTitle.set(title)
        viewModel.obsShowToolbar.set(showToolBar)
        viewModel.obsShowBackBtn.set(showBackBtn)

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (navController.currentDestination?.id == R.id.navigation_splash)
            finishAffinity()
    }
}