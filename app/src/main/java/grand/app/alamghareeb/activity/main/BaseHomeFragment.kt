package grand.app.alamghareeb.activity.main

import android.os.Bundle
import android.view.View
import androidx.activity.addCallback
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseFragment
import timber.log.Timber

open class BaseHomeFragment : BaseFragment() {
    override fun onDestroyView() {
        super.onDestroyView()
        when {
            !isNavigated -> requireActivity().onBackPressedDispatcher.addCallback(this) {
                val navController = findNavController()
                when {
                    navController.currentBackStackEntry?.destination?.id != null -> {
                        findNavController().navigateUp()
                    }
                    else -> navController.popBackStack()
                }
            }
        }
    }

    val navController by lazy {
        activity.let {
            Navigation.findNavController(it!!, R.id.nav_home_host_fragment)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


    fun showBottomBar(show: Boolean) {
        try {
            val navView: BottomNavigationView = requireActivity().findViewById(R.id.bottomBar)
            when {
                show -> {
                    navView.visibility = View.VISIBLE
                }
                else -> navView.visibility = View.GONE
            }
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
}

