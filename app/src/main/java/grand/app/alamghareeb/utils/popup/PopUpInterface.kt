package grand.app.alamghareeb.utils.popup

interface PopUpInterface {
    fun submitPopUp(position: Int)
}