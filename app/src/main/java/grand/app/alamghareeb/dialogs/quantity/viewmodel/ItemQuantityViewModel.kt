package grand.app.alamghareeb.dialogs.quantity.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.restoreorder.model.ReasonItem

class ItemQuantityViewModel(var item: Int) : BaseViewModel()
