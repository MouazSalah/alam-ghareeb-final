package grand.app.alamghareeb.dialogs.specialorder.model

import java.io.File

data class SpecialOrderRequest(
    var name: String? = null,
    var price: String? = null,
    var quantity: Int = 1,
    var description: String? = null,
    var product_image: File? = null
)