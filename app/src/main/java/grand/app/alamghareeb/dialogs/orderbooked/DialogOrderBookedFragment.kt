package grand.app.alamghareeb.dialogs.orderbooked

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogOrderBookedBinding
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params

class DialogOrderBookedFragment : DialogFragment() {
    lateinit var binding: DialogOrderBookedBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogOrderBookedBinding.inflate(inflater, container, false)

        binding.layoutBooked.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
            requireActivity().setResult(Codes.DIALOG_ORDER_SUCCESS, intent)
            requireActivity().finish()
        }

        return binding.root
    }
}