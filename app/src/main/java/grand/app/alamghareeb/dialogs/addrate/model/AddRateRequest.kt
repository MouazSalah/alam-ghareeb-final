package grand.app.alamghareeb.dialogs.addrate.model

data class AddRateRequest(
    var rate: Float? = null,
    var review: String? = null
)