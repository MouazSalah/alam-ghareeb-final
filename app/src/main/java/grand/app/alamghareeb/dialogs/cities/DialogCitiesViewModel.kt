package grand.app.alamghareeb.dialogs.cities

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.dialogs.language.LanguageAdapter
import grand.app.alamghareeb.dialogs.language.LanguageItem
import grand.app.alamghareeb.main.addresses.model.CityItem
import grand.app.alamghareeb.utils.constants.Codes

class DialogCitiesViewModel : BaseViewModel() {
    var adapter = CitiesAdapter()
    var cityItem = CityItem()

    fun onConfirmClicked() {
        when (adapter.selectedPosition) {
            -1 -> {
                setValue(Codes.EMPTY_SELECTED_CITY)
            }
            else -> {
                setValue(Codes.SELECT_LANGUAGE)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}