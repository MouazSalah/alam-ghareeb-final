package grand.app.alamghareeb.dialogs.toast

import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogToastBinding
import grand.app.alamghareeb.utils.constants.Params
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class DialogToastFragment : DialogFragment() {
    private lateinit var binding: DialogToastBinding
    var message = ""
    var toastType = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            if (requireArguments().containsKey(Params.DIALOG_TOAST_MESSAGE)) {
                message = requireArguments().getString(Params.DIALOG_TOAST_MESSAGE, "")
                toastType = requireArguments().getInt(Params.DIALOG_TOAST_TYPE, 1)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogToastBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvMessage.text = message

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = requireActivity().window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor =
                    ContextCompat.getColor(requireActivity(), R.color.color_primary)
            }
        }

        /*
        * toastType == 1 >> failed toast
        * toastType == 2 >> success toast
        */
        if (toastType == 1) {
            binding.ivToast.setImageResource(R.drawable.ic_toast_failed)
            binding.toastLayout.background =
                ContextCompat.getDrawable(requireActivity(), R.drawable.toast_failed_bg)
        } else {
            binding.ivToast.setImageResource(R.drawable.ic_toast_success)
            binding.toastLayout.background =
                ContextCompat.getDrawable(requireActivity(), R.drawable.toast_success_bg)
        }

        lifecycleScope.launch {
            delay(2000)
            requireActivity().finish()
        }
    }
}
