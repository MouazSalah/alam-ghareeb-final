package grand.app.alamghareeb.dialogs.cities

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogCitiesBinding
import grand.app.alamghareeb.main.addresses.model.CitiesResponse
import grand.app.alamghareeb.main.addresses.model.CityItem
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe

class DialogCitiesFragment : DialogFragment() {
    lateinit var binding: DialogCitiesBinding
    lateinit var viewModel: DialogCitiesViewModel
    var citiesResponse = CitiesResponse()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.CITIES_RESPONSE) -> {
                        citiesResponse =
                            requireArguments().getSerializable(Params.CITIES_RESPONSE) as CitiesResponse
                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogCitiesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogCitiesViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.adapter.updateList(citiesResponse.citiesList as ArrayList<CityItem>)

        observe(viewModel.adapter.itemLiveData) {
            viewModel.cityItem = it!!
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.EMPTY_SELECTED_CITY -> {
                    Toasty.error(requireActivity(), getString(R.string.msg_empty_city)).show()
                }
                Codes.SELECT_LANGUAGE -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.CITY_ITEM, viewModel.cityItem)
                    requireActivity().setResult(Codes.DIALOG_SELECT_CITY, intent)
                    requireActivity().finish()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.DIALOG_SELECT_CITY, intent)
                    requireActivity().finish()
                }
            }
        }
    }
}