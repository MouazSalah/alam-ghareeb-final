package grand.app.alamghareeb.dialogs.areas

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import es.dmoral.toasty.Toasty
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.DialogAreasBinding
import grand.app.alamghareeb.main.addresses.model.AreaItem
import grand.app.alamghareeb.main.addresses.model.AreasResponse
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe

class DialogAreasFragment : DialogFragment() {
    lateinit var binding: DialogAreasBinding
    lateinit var viewModel: DialogAreasViewModel
    var areasResponse = AreasResponse()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        when {
            arguments != null -> {
                when {
                    requireArguments().containsKey(Params.AREAS_RESPONSE) -> {
                        areasResponse =
                            requireArguments().getSerializable(Params.AREAS_RESPONSE) as AreasResponse
                    }
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogAreasBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogAreasViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.adapter.updateList(areasResponse.areasList as ArrayList<AreaItem>)

        observe(viewModel.adapter.itemLiveData) {
            viewModel.areaItem = it!!
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.EMPTY_SELECTED_AREA -> {
                    Toasty.error(requireActivity(), getString(R.string.msg_empty_area)).show()
                }
                Codes.SELECT_AREA -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.AREA_ITEM, viewModel.areaItem)
                    requireActivity().setResult(Codes.DIALOG_SELECT_AREA, intent)
                    requireActivity().finish()
                }
                Codes.CLOSE_CLICKED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    requireActivity().setResult(Codes.DIALOG_SELECT_AREA, intent)
                    requireActivity().finish()
                }
            }
        }
    }
}