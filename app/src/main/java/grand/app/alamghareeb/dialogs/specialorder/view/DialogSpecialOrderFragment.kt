package grand.app.alamghareeb.dialogs.specialorder.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.fxn.pix.Options
import com.fxn.pix.Pix
import grand.app.alamghareeb.databinding.DialogSpecialOrderBinding
import grand.app.alamghareeb.dialogs.specialorder.model.SpecialOrderResponse
import grand.app.alamghareeb.dialogs.specialorder.viewmodel.DialogSpecialOrderViewModel
import grand.app.alamghareeb.dialogs.toast.DialogToastFragment
import grand.app.alamghareeb.location.util.PermissionUtil
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class DialogSpecialOrderFragment : DialogFragment() {
    lateinit var binding: DialogSpecialOrderBinding
    lateinit var viewModel: DialogSpecialOrderViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogSpecialOrderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(DialogSpecialOrderViewModel::class.java)
        binding.viewModel = viewModel

        observe(viewModel.mutableLiveData)
        {
            when (it) {
                Codes.PICK_ORDER_IMAGE -> {
                    pickImage(Codes.PICK_ORDER_IMAGE)
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SpecialOrderResponse -> {
                            val intent = Intent()
                            intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                            requireActivity().setResult(Codes.DIALOG_SPECIAL_ORDER, intent)
                            requireActivity().finish()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Codes.PICK_ORDER_IMAGE -> {
                data?.let {
                    val returnValue = it.getStringArrayListExtra(Pix.IMAGE_RESULTS)
                    returnValue?.let { array ->
                        when (requestCode) {
                            Codes.PICK_ORDER_IMAGE -> {
                                val returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                                binding.imgProduct.setImageURI(returnValue[0].toUri())
                                binding.noImgLayout.visibility = View.GONE
                                binding.imgProduct.visibility = View.VISIBLE
                                viewModel.gotImage(requestCode, array[0])
                            }
                        }
                    }
                }
            }
        }
    }

    fun pickImage(requestCode: Int) {
        val options = Options.init()
            .setRequestCode(requestCode) //Request code for activity results
            .setFrontfacing(false) //Front Facing camera on start
            .setExcludeVideos(true) //Option to exclude videos
        if (PermissionUtil.hasImagePermission(requireActivity())) {
            Pix.start(this, options)
        } else {
            observe(
                PermissionUtil.requestPermission(
                    requireActivity(),
                    PermissionUtil.getImagePermissions()
                )
            ) {
                when (it) {
                    PermissionUtil.AppPermissionResult.AllGood -> Pix.start(
                        this,
                        options
                    )
                }
            }
        }
    }

    fun showToast(msg: String, type: Int) {
        val bundle = Bundle()
        bundle.putString(Params.DIALOG_TOAST_MESSAGE, msg)
        bundle.putInt(Params.DIALOG_TOAST_TYPE, type)
        Utils.startDialogActivity(requireActivity(),
            DialogToastFragment::class.java.name,
            Codes.DIALOG_TOAST_REQUEST,
            bundle)
    }
}