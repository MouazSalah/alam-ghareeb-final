package grand.app.alamghareeb.dialogs.restoresuccess

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.databinding.DialogRestoreSuccessBinding

class DialogRestoreSuccessFragment : DialogFragment() {
    lateinit var binding: DialogRestoreSuccessBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogRestoreSuccessBinding.inflate(inflater, container, false)

        binding.btnBrowse.setOnClickListener {
            requireActivity().startActivity(Intent(requireActivity(), MainActivity::class.java))
            requireActivity().finishAffinity()
        }

        return binding.root
    }
}