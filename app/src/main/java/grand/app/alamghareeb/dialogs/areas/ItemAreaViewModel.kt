package grand.app.alamghareeb.dialogs.areas

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.addresses.model.AreaItem
import grand.app.alamghareeb.main.addresses.model.CityItem

class ItemAreaViewModel(var item: AreaItem) : BaseViewModel()
