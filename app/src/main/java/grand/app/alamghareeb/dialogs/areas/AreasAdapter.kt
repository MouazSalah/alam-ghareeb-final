package grand.app.alamghareeb.dialogs.areas

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawAreaBinding
import grand.app.alamghareeb.databinding.RawCityBinding
import grand.app.alamghareeb.dialogs.cities.ItemCityViewModel
import grand.app.alamghareeb.main.addresses.model.AreaItem
import grand.app.alamghareeb.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class AreasAdapter : RecyclerView.Adapter<AreasAdapter.PaymentHolder>() {
    var itemsList: ArrayList<AreaItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<AreaItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawAreaBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_area, parent, false)
        return PaymentHolder(binding)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        val itemViewModel = ItemAreaViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos: Int): AreaItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<AreaItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PaymentHolder(val binding: RawAreaBinding) : RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 4
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_on_button)
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_off_button)
                }
            }
        }
    }
}
