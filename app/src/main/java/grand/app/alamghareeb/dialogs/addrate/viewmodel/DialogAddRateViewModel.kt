package grand.app.alamghareeb.dialogs.addrate.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.dialogs.addrate.model.AddRateRequest
import grand.app.alamghareeb.dialogs.addrate.model.AddRateResponse
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall

class DialogAddRateViewModel : BaseViewModel() {
    var request = AddRateRequest()
    var obsRate = ObservableField<Float>(5f)
    var id: Int = 0

    fun onAddClicked() {
        when (request.review) {
            null -> {
                setValue(Codes.EMPTY_MESSAGE)
            }
            else -> {
                request.rate = obsRate.get()
                addRate()
            }
        }
    }

    private fun addRate() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().requestPostBody(URLS.RATES + id + "/rate",
                request)
        }) { res ->
            obsIsProgress.set(false)
            val response: AddRateResponse = Gson().fromJson(res, AddRateResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }
}