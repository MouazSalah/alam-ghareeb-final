package grand.app.alamghareeb.dialogs.language

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawLanguageBinding
import grand.app.alamghareeb.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class LanguageAdapter : RecyclerView.Adapter<LanguageAdapter.PaymentHolder>() {
    var itemsList: ArrayList<LanguageItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<LanguageItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawLanguageBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_language, parent, false)
        return PaymentHolder(binding)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        val itemViewModel = ItemLanguageViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos: Int): LanguageItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<LanguageItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PaymentHolder(val binding: RawLanguageBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.dayLayout.strokeWidth = 4
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_on_button)
                }
                else -> {
                    binding.dayLayout.strokeWidth = 0
                    binding.ivSelected.setImageResource(R.drawable.ic_radio_off_button)
                }
            }
        }
    }
}
