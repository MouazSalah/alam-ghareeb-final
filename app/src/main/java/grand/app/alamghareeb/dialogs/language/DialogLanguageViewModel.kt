package grand.app.alamghareeb.dialogs.language

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.utils.constants.Codes

class DialogLanguageViewModel : BaseViewModel() {
    var adapter = LanguageAdapter()
    var language: String? = null

    init {
        val paymentList = arrayListOf(
            LanguageItem(0, "عربي", "ar"),
            LanguageItem(1, "English", "en")
        )

        adapter.updateList(paymentList)
    }

    fun onConfirmClicked() {
        when (language) {
            null, "" -> {
                setValue(Codes.EMPTY_SELECTED_LANGUAGE)
            }
            else -> {
                setValue(Codes.SELECT_LANGUAGE)
            }
        }
    }

    fun onCloseClicked() {
        setValue(Codes.CLOSE_CLICKED)
    }
}