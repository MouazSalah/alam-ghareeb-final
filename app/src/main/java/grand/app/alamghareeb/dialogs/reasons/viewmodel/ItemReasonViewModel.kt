package grand.app.alamghareeb.dialogs.reasons.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.restoreorder.model.ReasonItem

class ItemReasonViewModel(var item: ReasonItem) : BaseViewModel()
