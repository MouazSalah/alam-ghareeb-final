package grand.app.alamghareeb.network

object URLS {
    const val BASE_URL = "https://tawous.my-staff.net"

    const val MAIN_SETTINGS: String = "Main-Setting"
    const val USER_SIZES: String = "user-sizes"
    const val LOGIN: String = "/api/v1/login"
    const val REGISTER: String = "/api/v1/register"
    const val FORGOT_PASSWORD: String = "/api/v1/code_send"
    const val VERIFY_CODE: String = "/api/v1/code_check"
    const val CHANGE_PASSWORD: String = "/api/v1/change_password"
    const val UPDATE_PROFILE: String = "user/update-profile"
    const val UPDATE_TOKEN: String = "user/token"
    const val LOGOUT: String = "/api/v1/logout"

    const val HOME: String = "/api/v1/home"
    const val OFFERS: String = "/api/v1/offers"
    const val NOTIFICATIONS: String = "/api/v1/notifications"
    const val WALLET: String = "/api/v1/wallet"

    const val FAVORITES: String = "/api/v1/favorite"
    const val CARTS: String = "/api/v1/cart"

    const val ALL_CATEGORIES: String = "api/v1/all_categories"
    const val CATEGORIES_DETAILS: String = "api/v1/categories"

    const val PRODUCTS: String = "api/v1/products"
    const val PRODUCT_DETAILS: String = "/api/v1/product"
    const val MY_ORDERS: String = "/api/v1/orders"

    const val ABOUT_APP: String = "/api/v1/about_us"
    const val TERMS_AND_CONDITIONS: String = "/api/v1/terms_condition"
    const val PRIVACY_POLICY: String = "/api/v1/privacy_policy"
    const val SEND_SUGGEST: String = "/api/v1/complaints_suggestions"
    const val CONTACT_US: String = "/api/v1/contact_us"
    const val ORDER_DETAILS: String = "/api/v1/order/"
    const val CANCEL_ORDER: String = "/api/v1/order/"
    const val RESTORE_REASONS: String = "/api/v1/restore_reasons"
    const val RESTORE_ORDER: String = "/api/v1/order/"

    const val TOP_RATED: String = "/api/v1/products/top_rated"
    const val BEST_OFFERS: String = "/api/v1/products/best_offers"
    const val MOST_SELLING: String = "/api/v1/products/most_selling"
    const val ORDERED_BEFORE: String = "/api/v1/products/ordered_before"
    const val FILTER_ORDERS: String = "/api/v1/orders/order_by"
    const val FILTER_PRODUCTS: String = "/api/v1/order_by"

    const val FILTER_PRODUCTS_RESULT: String = "/api/v1/filter/result"

    const val ADD_TO_CART: String = "/api/v1/cart"
    const val BRANCHES: String = "/api/v1/branches"
    const val RATES: String = "/api/v1/product/"

    const val ALL_ADDRESSES: String = "/api/v1/addresses"
    const val ADD_ADDRESS: String = "/api/v1/address"
    const val DELETE_ADDRESS: String = "/api/v1/address"
    const val SET_DEFAULT: String = "/api/v1/address/default"
    const val ALL_AREAS: String = "/api/v1/areas"
    const val ALL_CITIES: String = "/api/v1/cities"
    const val CONFIRM_ORDER: String = "/api/v1/order"
    const val APPLY_COUPON: String = "/api/v1/promo_code"
    const val FILTER_DETAILS: String = "/api/v1/filter/details"

    const val MAKE_ORDER: String = "/api/v1/cities"
    const val SPECIAL_ORDER: String = "/api/v1/special_order"
}