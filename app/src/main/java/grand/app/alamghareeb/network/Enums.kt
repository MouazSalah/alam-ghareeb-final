package grand.app.alamghareeb.network

/**
 * Created by MouazSalah 28/12/2020.
 **/
enum class Status {
    SUCCESS,
    SUCCESS_MESSAGE,
    ERROR_MESSAGE,
    NOT_LOGIN,
    SHIMMER_LOADING,
    PROGRESS_LOADING
}

