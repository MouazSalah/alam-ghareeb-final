package grand.app.alamghareeb.network

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Params
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
/* Updated by MouazSalah 23/06/2021*/
 */

@Module
class ConnectionModule {
    private var retrofit: Retrofit? = null

    companion object {
        var bufferSize = 256 * 1024
        private val TAG: String = "ConnectionModule"
    }

    @Singleton
    @Provides
    fun webService(): ApiInterface? {
        when (retrofit) {
            null -> {
                val gSon = GsonBuilder()
                    .setLenient()
                    .create()
                retrofit = Retrofit.Builder()
                    .baseUrl(URLS.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gSon))
                    .client(createOkHttpClient())
                    .build()
            }
        }
        return retrofit?.create(ApiInterface::class.java)
    }

    @Singleton
    @Provides
    fun createOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .apply {
                this.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .addInterceptor(Interceptor { chain ->
                        when {
                            PrefMethods.getUserData() != null -> {
                                val request: Request = chain.request().newBuilder()
                                    .addHeader("language", PrefMethods.getLanguage())
                                    .addHeader("Accept", "application/json")
                                    .addHeader("Authorization",
                                        "Bearer " + PrefMethods.getUserData()?.jwt)
                                    .build()
                                chain.proceed(request)
                            }
                            Params.token != "" && Params.token != null -> {
                                Timber.e("mou3az_header : asked to login")
                                val request: Request = chain.request().newBuilder()
                                    .addHeader("language", PrefMethods.getLanguage())
                                    .addHeader("Accept", "application/json")
                                    .addHeader("Authorization", "Bearer " + Params.token)
                                    .build()
                                chain.proceed(request)
                            }
                            else -> {
                                val request: Request = chain.request().newBuilder()
                                    .addHeader("language", PrefMethods.getLanguage()).build()
                                chain.proceed(request)
                            }
                        }
                    })
            }
            .readTimeout(20, TimeUnit.MINUTES)
            .connectTimeout(20, TimeUnit.MINUTES)
            .writeTimeout(120, TimeUnit.SECONDS)
            .build()
    }
}
