package grand.app.alamghareeb.network

object ServerStatus {
    const val SUCCESS = 200
    const val FAIL = 404
    const val NOT_AUTH = 401
}