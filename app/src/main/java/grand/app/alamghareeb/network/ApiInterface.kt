package grand.app.alamghareeb.network

import com.google.gson.JsonObject
import grand.app.alamghareeb.dialogs.specialorder.model.SpecialOrderResponse
import grand.app.alamghareeb.main.editprofile.response.UpdateProfileResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiInterface {
    /* Post Request with only image file */
    @Multipart
    @POST
    suspend fun requestPostMultiPart(
        @Url url: String?,
        @QueryMap map: MutableMap<String?, String?>?,
        @Part file: MultipartBody.Part?
    ): JsonObject

    @Multipart
    @POST
    suspend fun requestMultiPartObject(
        @Url url: String?,
        @Body `object`: Any?,
        @Part file: MultipartBody.Part?
    ): JsonObject


    @Multipart
    @POST("/api/v1/profile")
    suspend fun updateProfile(
        @Part("name") name: RequestBody?,
        @Part("email") email: RequestBody?,
        @Part("phone") phone: RequestBody?,
        @Part image: MultipartBody.Part?
    ): UpdateProfileResponse

    /* Post Request with Multi image file */
    @POST
    suspend fun post(
        @Url url: String?,
        @QueryMap map: MutableMap<String?, String?>?,
        @Body file: RequestBody?
    ): JsonObject

    /* Post Request with Query Map Request */
    @POST
    suspend fun post(@Url url: String?, @QueryMap map: MutableMap<String?, String?>?): JsonObject

    /* Post Request with object body request */
    @POST
    suspend fun post(@Url url: String?, @Body `object`: Any?): JsonObject

    @GET
    suspend fun get(@Url url: String?): JsonObject

    @GET
    suspend fun getQuery(@Url url: String?, @Query("city_id") cityId: Int?): JsonObject

    @GET
    suspend fun filterOrders(@Url url: String?, @Query("order_by") orderBy: String?): JsonObject

    @GET
    suspend fun filterProducts(@Url url: String?, @Query("order_by") orderBy: String?): JsonObject

    @GET
    suspend fun applyCoupon(@Url url: String?, @Query("code") code: String?): JsonObject

    @GET
    suspend fun getProductsByCategory(@Url url: String?, @Query("category_id") id: Int?): JsonObject

    @GET
    suspend fun getFilterDetails(@Url url: String?, @Query("category_id") id: Int?): JsonObject

    @GET
    suspend fun getProductsBySubCategory(
        @Url url: String?,
        @Query("category_id") catId: Int?,
        @Query("sub_category_id") subCatId: Int?
    ): JsonObject

    @GET
    suspend fun get(@Url url: String?, @QueryMap map: MutableMap<String?, String?>?): JsonObject

    @DELETE
    suspend fun delete(@Url url: String?, @QueryMap map: MutableMap<String?, String?>?): JsonObject

    @DELETE
    suspend fun deleteQuery(@Url url: String?, @Query("address_id") id: Int): JsonObject

    @Multipart
    @POST("/api/v1/special_order")
    suspend fun specialOrder(
        @Part("name") name: RequestBody?,
        @Part("price") email: RequestBody?,
        @Part("quantity") phone: RequestBody?,
        @Part("description") password: RequestBody?,
        @Part image: MultipartBody.Part?
    ): SpecialOrderResponse
}