package grand.app.alamghareeb.main.orderdetails.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentOrderDetailsBinding
import grand.app.alamghareeb.dialogs.confirm.DialogConfirmFragment
import grand.app.alamghareeb.location.map.MapsActivity
import grand.app.alamghareeb.main.orderdetails.model.CancelOrderResponse
import grand.app.alamghareeb.main.orderdetails.model.OrderDetailsResponse
import grand.app.alamghareeb.main.orderdetails.viewmodel.OrderDetailsViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import grand.app.alamghareeb.utils.resources.ResourceManager

class OrderDetailsFragment : BaseFragment() {
    lateinit var binding: FragmentOrderDetailsBinding
    lateinit var viewModel: OrderDetailsViewModel
    val fragmentArgs: OrderDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_order_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(OrderDetailsViewModel::class.java)
        binding.viewModel = viewModel

        binding.tvDesc.text = "( ${fragmentArgs.orderId} )"

        viewModel.getOrderDetails(fragmentArgs.orderId)

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.tvBranchMap.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(Params.BRANCH_NAME, viewModel.orderDetails.branchName)
            bundle.putDouble(Params.BRANCH_LAT, viewModel.orderDetails.branchLatitude!!.toDouble())
            bundle.putDouble(Params.BRANCH_LONG,
                viewModel.orderDetails.branchLongitude!!.toDouble())
            requireActivity().startActivityForResult(Intent(requireActivity(),
                MapsActivity::class.java).putExtras(bundle), Codes.ADD_ADDRESS_REQUEST_CODE)
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is CancelOrderResponse -> {
                            viewModel.getOrderDetails(fragmentArgs.orderId)
                        }
                        is OrderDetailsResponse -> {
                            when (it.data.orderDetailsData?.historyList?.size) {
                                1 -> {
                                    binding.ivFirstStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivSecondStep.setImageResource(R.drawable.ic_order_unselected)
                                    binding.ivThirdStep.setImageResource(R.drawable.ic_order_unselected)
                                    binding.ivFourthStep.setImageResource(R.drawable.ic_order_unselected)

                                    binding.ivOrderAccepted.background =
                                        ResourceManager.getDrawable(R.drawable.unselected_status)
                                    binding.ivOrderPrepared.background =
                                        ResourceManager.getDrawable(R.drawable.unselected_status)
                                    binding.ivOrderCompleted.background =
                                        ResourceManager.getDrawable(R.drawable.unselected_status)
                                }
                                2 -> {
                                    binding.ivFirstStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivSecondStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivThirdStep.setImageResource(R.drawable.ic_order_unselected)
                                    binding.ivFourthStep.setImageResource(R.drawable.ic_order_unselected)
                                    binding.ivOrderAccepted.background =
                                        ResourceManager.getDrawable(R.drawable.selected_status)
                                    binding.ivOrderPrepared.background =
                                        ResourceManager.getDrawable(R.drawable.unselected_status)
                                    binding.ivOrderCompleted.background =
                                        ResourceManager.getDrawable(R.drawable.unselected_status)
                                }
                                3 -> {
                                    binding.ivFirstStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivSecondStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivThirdStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivFourthStep.setImageResource(R.drawable.ic_order_unselected)
                                    binding.ivOrderAccepted.background =
                                        ResourceManager.getDrawable(R.drawable.selected_status)
                                    binding.ivOrderPrepared.background =
                                        ResourceManager.getDrawable(R.drawable.selected_status)
                                    binding.ivOrderCompleted.background =
                                        ResourceManager.getDrawable(R.drawable.unselected_status)
                                }
                                4 -> {
                                    binding.ivFirstStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivSecondStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivThirdStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivFourthStep.setImageResource(R.drawable.ic_branch_checked)
                                    binding.ivOrderAccepted.background =
                                        ResourceManager.getDrawable(R.drawable.selected_status)
                                    binding.ivOrderPrepared.background =
                                        ResourceManager.getDrawable(R.drawable.selected_status)
                                    binding.ivOrderCompleted.background =
                                        ResourceManager.getDrawable(R.drawable.selected_status)
                                }
                            }

                            when {
                                it.data.orderDetailsData?.addresses == null || it.data.orderDetailsData.phone == null -> {
                                    binding.tvAddress.text = getString(R.string.label_not_found)
                                    binding.tvPhone.text = getString(R.string.label_not_found)
                                }
                            }

                            when (it.data.orderDetailsData?.payType) {
                                "branch" -> {
                                    binding.tvTypeStatus.text = getString(R.string.label_watitng_to_deliver)
                                    binding.tvBranchMap.visibility = View.VISIBLE
                                    binding.tvHome.visibility = View.GONE
                                    binding.addressLayout.visibility = View.GONE
                                    binding.phoneLayout.visibility = View.GONE
                                }
                                else -> {
                                    binding.tvTypeStatus.text = getString(R.string.label_on_the_way)
                                    binding.tvHome.visibility = View.VISIBLE
                                    binding.tvBranchMap.visibility = View.GONE
                                    binding.addressLayout.visibility = View.VISIBLE
                                    binding.phoneLayout.visibility = View.VISIBLE
                                }
                            }
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.CANCEL_CLICKED -> {
                                    Utils.startDialogActivity(requireActivity(),
                                        DialogConfirmFragment::class.java.name,
                                        Codes.DIALOG_CONFIRM_REQUEST,
                                        null)
                                }
                                Codes.RESTORE_CLICKED -> {
                                    findNavController().navigate(OrderDetailsFragmentDirections.orderDetailsToReturn(
                                        viewModel.orderDetails))
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.DIALOG_CONFIRM_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        viewModel.cancelOrder(fragmentArgs.orderId)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}