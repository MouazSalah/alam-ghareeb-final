package grand.app.alamghareeb.main.changepassword.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import grand.app.alamghareeb.main.changepassword.viewmodel.ChangePassViewModel
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.auth.forgotpass.model.ForgotPassResponse
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentChangePasswordBinding
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class ChangePassFragment : BaseFragment() {
    lateinit var binding: FragmentChangePasswordBinding

    @Inject
    lateinit var viewModel: ChangePassViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        binding.btnBrowse.setOnClickListener {
            findNavController().navigate(R.id.navigation_home)
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is ForgotPassResponse -> {
                            binding.detailsLayout.visibility = View.GONE
                            binding.layoutChanged.visibility = View.VISIBLE
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.LOGIN_CLICKED -> {
                                    Const.isAskedToLogin = 1
                                    requireActivity().startActivity(Intent(requireActivity(),
                                        MainActivity::class.java).putExtra(Const.ACCESS_LOGIN,
                                        true))
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}