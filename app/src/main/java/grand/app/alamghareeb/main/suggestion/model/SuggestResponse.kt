package grand.app.alamghareeb.main.suggestion.model

import com.google.gson.annotations.SerializedName

data class SuggestResponse(

    @field:SerializedName("message")
    val msg: String? = null,

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("status")
    val status: String? = null
)
