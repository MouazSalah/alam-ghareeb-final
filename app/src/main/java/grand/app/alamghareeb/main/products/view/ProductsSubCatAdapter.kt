package grand.app.alamghareeb.main.products.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.databinding.RawProductSubcategoryBinding
import grand.app.alamghareeb.main.products.model.SubCategoriesItem
import grand.app.alamghareeb.main.products.viewmodel.ItemProductSubCategoryViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import grand.app.alamghareeb.utils.resources.ResourceManager
import java.util.*

class ProductsSubCatAdapter : RecyclerView.Adapter<ProductsSubCatAdapter.GroupsHolder>() {
    var itemsList: ArrayList<SubCategoriesItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<SubCategoriesItem>()
    var selectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawProductSubcategoryBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_product_subcategory, parent, false)
        return GroupsHolder(binding)
    }

    override fun onBindViewHolder(holder: GroupsHolder, position: Int) {
        val itemViewModel = ItemProductSubCategoryViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                selectedPosition != position -> {
                    itemLiveData.value = itemViewModel.item
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SubCategoriesItem>) {
        itemsList.clear()
        itemsList.add(SubCategoriesItem(name = "الكل", id = 0))
        itemsList.addAll(models)
        notifyDataSetChanged()
    }

    inner class GroupsHolder(val binding: RawProductSubcategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.rawLayout.background =
                        ResourceManager.getDrawable(R.drawable.selected_subcateg)
                    binding.tvName.setTextColor(ContextCompat.getColor(BaseApp.getInstance,
                        R.color.white))
                }
                else -> {
                    binding.rawLayout.background =
                        ResourceManager.getDrawable(R.drawable.unselected_subcateg)
                    binding.tvName.setTextColor(ContextCompat.getColor(BaseApp.getInstance,
                        R.color.black))
                }
            }
        }
    }
}
