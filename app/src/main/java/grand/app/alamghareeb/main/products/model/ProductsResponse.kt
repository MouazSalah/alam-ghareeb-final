package grand.app.alamghareeb.main.products.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.home.model.SliderItem
import grand.app.alamghareeb.main.orders.model.Links
import grand.app.alamghareeb.main.orders.model.Meta

data class ProductsResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val productsCategoryData: ProductsCategoryData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class ProductsData(

    @field:SerializedName("data")
    val productsList: List<ProductItem?>? = null,

    @field:SerializedName("meta")
    val meta: Meta? = null,

    @field:SerializedName("links")
    val links: Links? = null
)

data class ProductsCategoryData(

    @field:SerializedName("banner")
    val slidersList: List<SliderItem?>? = null,

    @field:SerializedName("categories")
    val categoriesList: List<CategoriesItem?>? = null,

    @field:SerializedName("category")
    val productCategoryData: ProductCategoryData? = null
)

data class SubCategoriesItem(

    @field:SerializedName("image")
    val image: Any? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class ProductCategoryData(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("sub_categories")
    val subCategoriesList: List<SubCategoriesItem?>? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("products")
    val productsData: ProductsData? = null
)