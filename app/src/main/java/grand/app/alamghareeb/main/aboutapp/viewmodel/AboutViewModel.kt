package grand.app.alamghareeb.main.aboutapp.viewmodel

import android.text.Html
import android.text.TextUtils
import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.aboutapp.model.AboutResponse
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class AboutViewModel @Inject constructor() : BaseViewModel() {
    var obsText = ObservableField<String>()

    fun getDetails(url: String) {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(url) }) { res ->
            val response: AboutResponse = Gson().fromJson(res, AboutResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    val spanned = Html.fromHtml(response.aboutData?.value)
                    val chars = CharArray(spanned.length)
                    TextUtils.getChars(spanned, 0, spanned.length, chars, 0)
                    val plainText = String(chars)

                    obsText.set(plainText)
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }
}