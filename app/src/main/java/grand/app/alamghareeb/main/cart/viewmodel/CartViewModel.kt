package grand.app.alamghareeb.main.cart.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.cart.adapter.DeliveryWaysAdapter
import grand.app.alamghareeb.main.cart.model.*
import grand.app.alamghareeb.main.cart.view.CartAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class CartViewModel @Inject constructor() : BaseViewModel() {
    var adapter = CartAdapter()
    var deliveryAdapter = DeliveryWaysAdapter()
    private val cartsList: ArrayList<CartItem> = PrefMethods.getUserCarts()!!
    var obsTotalPrice = ObservableField(0.0)

    fun getCarts() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.BRANCHES) }) { res ->
            val response: BranchesResponse = Gson().fromJson(res, BranchesResponse::class.java)
            when {
                PrefMethods.getUserCarts()
                    .isNullOrEmpty() || PrefMethods.getUserCarts()?.size == 0 -> {
                    obsLayout.set(LoadingStatus.EMPTY)
                }
                else -> {
                    obsLayout.set(LoadingStatus.FULL)
                    adapter.updateList(PrefMethods.getUserCarts() as ArrayList<CartItem>)
                    for (i in cartsList) {
                        val itemTotal = i.quantity?.toDouble()?.times(i.price!!)
                        obsTotalPrice.set(itemTotal?.let { obsTotalPrice.get()?.plus(it) })
                    }
                }
            }
            when (response.code) {
                200 -> {
                    if (response.branchesList!!.isNotEmpty()) {
                        setDeliveryWays(response.branchesList as ArrayList<BranchItem>)
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getCarts()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    private fun setDeliveryWays(list: ArrayList<BranchItem>) {
        val list = arrayListOf(
            DeliveryWayItem(1, getString(R.string.delivering_from_branches), list),
            DeliveryWayItem(0, getString(R.string.delivering_from_home), null)
        )

        deliveryAdapter.updateList(list)
        notifyChange()
    }

    fun onOrderClicked() {
        when {
            PrefMethods.getUserData() == null -> {
                apiResponseLiveData.value = ApiResponse.success(Codes.NOT_LOGIN)
            }
            else -> {
                when {
                    deliveryAdapter.selectedPosition == -1 -> {
                        apiResponseLiveData.value =
                            ApiResponse.errorMessage(getString(R.string.msg_please_select_delivery_way))
                    }
                    deliveryAdapter.selectedPosition == 0 && deliveryAdapter.branchId == -1 -> {
                        apiResponseLiveData.value =
                            ApiResponse.errorMessage(getString(R.string.msg_please_select_branch_item))
                    }
                    else -> {
                        apiResponseLiveData.value = ApiResponse.success(Codes.CONFIRM_ORDER)
                    }
                }
            }
        }
    }

    fun inCreaseCartCount(item: CartItem) {
        cartsList[adapter.itemsList.indexOf(item)].quantity = item.quantity?.plus(1)
        cartsList[adapter.itemsList.indexOf(item)].total =
            item.price?.times(adapter.itemsList[adapter.itemsList.indexOf(item)].quantity!!)
        adapter.updateList(cartsList)
        obsTotalPrice.set(obsTotalPrice.get()!!.toDouble().plus(item.price!!))
        PrefMethods.saveUserCarts(cartsList)
    }

    fun deCreaseCartCount(item: CartItem) {
        cartsList[adapter.itemsList.indexOf(item)].quantity = item.quantity?.minus(1)
        cartsList[adapter.itemsList.indexOf(item)].total =
            item.price?.times(cartsList[cartsList.indexOf(item)].quantity!!)
        adapter.updateList(cartsList)
        obsTotalPrice.set(obsTotalPrice.get()!!.toDouble().minus(item.price!!))
        PrefMethods.saveUserCarts(cartsList)
    }

    fun removeItemFromCart(item: CartItem) {
        cartsList.remove(item)
        adapter.itemsList.remove(item)
        PrefMethods.saveUserCarts(cartsList)
        adapter.notifyDataSetChanged()
        obsTotalPrice.set(obsTotalPrice.get()!!.toDouble() - item.total!!)

        when (cartsList.size) {
            0 -> {
                obsTotalPrice.set(0.0)
                PrefMethods.deleteUserCarts()
                obsLayout.set(LoadingStatus.EMPTY)
            }
        }
        apiResponseLiveData.value = ApiResponse.success(Codes.ITEM_CART_REMOVED)
    }
}