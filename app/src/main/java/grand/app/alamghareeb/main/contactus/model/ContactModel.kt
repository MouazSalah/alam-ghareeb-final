package grand.app.alamghareeb.main.contactus.model

import android.graphics.drawable.Drawable

class ContactModel(
    var id: Int? = null,
    var title: String? = null,
    var image: Drawable? = null,
    var link: String? = null
)