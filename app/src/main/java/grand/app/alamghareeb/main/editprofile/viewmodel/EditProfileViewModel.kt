package grand.app.alamghareeb.main.editprofile.viewmodel

import androidx.databinding.ObservableField
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.location.util.AddressItem
import grand.app.alamghareeb.main.editprofile.request.EditProfileRequest
import grand.app.alamghareeb.main.editprofile.response.UpdateProfileResponse
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import grand.app.alamghareeb.utils.stringPathToFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class EditProfileViewModel : BaseViewModel() {
    var request = EditProfileRequest()
    var obsUserImg = ObservableField<Any>()
    var obsShowEditBtn = ObservableField<Boolean>()

    fun onUpdateClicked() {
        when {
            request.name == null || request.phone == null || request.email == null -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_invalide_data))
            }
            request.name.isNullOrEmpty() || request.phone.isNullOrEmpty() || request.email.isNullOrEmpty() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_invalide_data))
            }
            else -> {
                setClickable()
                updateProfile()
            }
        }
    }

    fun onChooseImgClicked() {
        setClickable()
        setValue(Codes.CHOOSE_IMAGE_CLICKED)
    }

    private fun updateProfile() {

        obsIsProgress.set(true)
        requestCall<UpdateProfileResponse?>({
            withContext(Dispatchers.IO) { return@withContext getApiRepo().updateProfile(request) }
        })
        { res ->
            obsIsProgress.set(false)
            when (res!!.code) {
                200 -> {
                    PrefMethods.saveUserData(res.userData)
                    apiResponseLiveData.value = ApiResponse.success(res)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(res.message)
                }
            }
        }
    }

    fun gotImage(requestCode: Int, path: String) {
        when (requestCode) {
            Codes.USER_PROFILE_IMAGE_REQUEST -> {
                request.userImg = path.stringPathToFile()
                notifyChange()
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                obsLayout.set(LoadingStatus.FULL)
                request.name = PrefMethods.getUserData()!!.name.toString()
                request.email = PrefMethods.getUserData()!!.email.toString()
                request.phone = PrefMethods.getUserData()!!.phone.toString()
                obsUserImg.set(PrefMethods.getUserData()?.image.toString())
                obsShowEditBtn.set(true)
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
                obsShowEditBtn.set(false)
            }
        }
    }

    init {
        getUserStatus()
    }
}