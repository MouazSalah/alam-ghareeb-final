package grand.app.alamghareeb.main.restoreorder.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawRestoreProductBinding
import grand.app.alamghareeb.main.orders.model.OrderProductItem
import grand.app.alamghareeb.main.restoreorder.viewmodel.ItemRestoreProductViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class RestoreProductsAdapter : RecyclerView.Adapter<RestoreProductsAdapter.AddressHolder>() {
    var itemsList = ArrayList<OrderProductItem>()
    var itemLiveData = SingleLiveEvent<OrderProductItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawRestoreProductBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_restore_product, parent, false)
        return AddressHolder(binding)
    }

    override fun onBindViewHolder(holder: AddressHolder, position: Int) {
        val itemViewModel = ItemRestoreProductViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.ibCheck.setOnClickListener {
            when {
                selectedPosition != position -> {
                    itemLiveData.value = itemViewModel.item
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<OrderProductItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    fun getItem(pos: Int): OrderProductItem {
        return itemsList[pos]
    }

    inner class AddressHolder(val binding: RawRestoreProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_checked)
                }
                else -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_unchecked)
                }
            }
        }
    }
}
