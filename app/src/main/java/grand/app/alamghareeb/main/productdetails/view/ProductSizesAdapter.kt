package grand.app.alamghareeb.main.productdetails.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawSizeBinding
import grand.app.alamghareeb.main.productdetails.model.ProductSizeItem
import grand.app.alamghareeb.main.productdetails.viewmodel.ItemSizeViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import grand.app.alamghareeb.utils.resources.ResourceManager
import java.util.*

class ProductSizesAdapter : RecyclerView.Adapter<ProductSizesAdapter.ProductColorsHolder>() {
    var itemsList: ArrayList<ProductSizeItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<ProductSizeItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductColorsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawSizeBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_size, parent, false)
        return ProductColorsHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductColorsHolder, position: Int) {
        val itemViewModel = ItemSizeViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.sizeLayout.setOnClickListener {
            when {
                selectedPosition != position -> {
                    itemLiveData.value = itemViewModel.item
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                }
                else -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ProductSizeItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class ProductColorsHolder(val binding: RawSizeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.sizeLayout.background =
                        ResourceManager.getDrawable(R.drawable.selected_size_bg)
                    binding.tvTitle.setTextColor(ResourceManager.getColor(R.color.white)!!)
                }
                else -> {
                    binding.sizeLayout.background =
                        ResourceManager.getDrawable(R.drawable.unselected_size_bg)
                    binding.tvTitle.setTextColor(ResourceManager.getColor(R.color.black)!!)
                }
            }
        }
    }
}
