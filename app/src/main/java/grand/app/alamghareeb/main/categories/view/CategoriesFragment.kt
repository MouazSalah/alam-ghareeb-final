package grand.app.alamghareeb.main.categories.view

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.main.BaseHomeFragment
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentCategoriesBinding
import grand.app.alamghareeb.main.categories.model.CategoriesResponse
import grand.app.alamghareeb.main.categories.viewmodel.CategoriesViewModel
import grand.app.alamghareeb.main.home.model.SliderItem
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class CategoriesFragment : BaseHomeFragment() {

    private lateinit var binding: FragmentCategoriesBinding

    @Inject
    lateinit var viewModel: CategoriesViewModel
    lateinit var sliderView: SliderView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_categories, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getCategories()
        }


        observe(viewModel.slidersAdapter.itemLiveData) {
            it?.let { item ->
                findNavController().navigate(CategoriesFragmentDirections.categoriesToProducts(item.categoryId!!))
            }
        }

        observe(viewModel.categoriesAdapter.itemLiveData) {
            it?.let { item ->
                findNavController().navigate(CategoriesFragmentDirections.categoriesToProducts(item.id!!))
            }
        }

        binding.etSearch.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                viewModel.onSearchClicked()
                return@OnEditorActionListener true
            }
            false
        })

        observe(viewModel.apiResponseLiveData) { it ->
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is CategoriesResponse -> {
                            setupSlider(it.data.categoriesData!!.slidersList as ArrayList<SliderItem>)
                        }
                        is Int -> {
                            when (it.data) {
                                Codes.SEARCH_CLICKED -> {
                                    findNavController().navigate(R.id.categories_to_search)
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    private fun setupSlider(covers: ArrayList<SliderItem>) {
        viewModel.slidersAdapter.updateList(covers)
        sliderView = binding.sliderCategories
        sliderView.setSliderAdapter(viewModel.slidersAdapter)
        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
        sliderView.indicatorSelectedColor =
            ContextCompat.getColor(requireActivity(), R.color.color_primary)
        sliderView.indicatorUnselectedColor = Color.WHITE
        sliderView.scrollTimeInSec = 3
        sliderView.isAutoCycle = true
        sliderView.startAutoCycle()
    }
}