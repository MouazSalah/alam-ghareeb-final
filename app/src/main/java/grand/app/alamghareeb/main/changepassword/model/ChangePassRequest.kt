package grand.app.alamghareeb.main.changepassword.model

data class ChangePassRequest(
    var phone: String? = null,
    var password: String? = null,
    var old_password: String? = null,
    var password_confirmation: String? = null
)