package grand.app.alamghareeb.main.adapter.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.smarteist.autoimageslider.SliderViewAdapter
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.databinding.RawSliderBinding
import grand.app.alamghareeb.main.adapter.viewmodel.ItemSliderViewModel
import grand.app.alamghareeb.main.home.model.SliderItem
import grand.app.alamghareeb.utils.SingleLiveEvent

class SlidersAdapter : SliderViewAdapter<SlidersAdapter.BannersHolder>() {
    var itemsList = ArrayList<SliderItem>()
    var itemLiveData = SingleLiveEvent<SliderItem>()

    override fun onCreateViewHolder(parent: ViewGroup?): BannersHolder {
        val context = parent!!.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawSliderBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_slider, parent, false)
        return BannersHolder(binding)
    }

    override fun onBindViewHolder(holder: BannersHolder, position: Int) {
        val itemViewModel = ItemSliderViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        Glide.with(BaseApp.getInstance).load(itemViewModel.item.image)
            .placeholder(R.drawable.ic_logo).into(holder.binding.imgSlider)

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    fun updateList(models: ArrayList<SliderItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class BannersHolder(val binding: RawSliderBinding) :
        SliderViewAdapter.ViewHolder(binding.root)

    override fun getCount(): Int {
        return itemsList.size
    }
}
