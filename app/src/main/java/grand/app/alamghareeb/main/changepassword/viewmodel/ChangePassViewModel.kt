package grand.app.alamghareeb.main.changepassword.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.auth.forgotpass.model.ForgotPassResponse
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.changepassword.model.ChangePassRequest
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class ChangePassViewModel @Inject constructor() : BaseViewModel() {
    var request = ChangePassRequest()

    fun onChangeClicked() {
        setClickable()
        when {
            request.password.isNullOrEmpty() || request.password.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_password))
            }
            request.password!!.length < 6 -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_invalid_password))
            }
            request.password_confirmation.isNullOrEmpty() || request.password_confirmation.isNullOrBlank() -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_confirm_password))
            }
            request.password_confirmation != request.password -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_not_match))
            }
            else -> {
                resetPassword()
            }
        }
    }

    private fun resetPassword() {
        //val resetRequest = ChangePassRequest(PrefMethods.getUserData()?.phone, request.password, request.old_password)
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().requestPostBody(URLS.CHANGE_PASSWORD,
                request)
        }) { res ->
            obsIsProgress.set(false)
            val response: ForgotPassResponse = Gson().fromJson(res, ForgotPassResponse::class.java)
            when (response.code) {
                200, 405 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onBrowseClicked() {

    }
}