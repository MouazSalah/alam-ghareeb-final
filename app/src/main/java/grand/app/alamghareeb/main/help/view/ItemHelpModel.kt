package grand.app.alamghareeb.main.help.view

import android.graphics.drawable.Drawable

data class ItemHelpModel(
    var id: Int? = null,
    var title: String? = null,
    var image: Drawable? = null
)
