package grand.app.alamghareeb.main.orders.viewmodel

import androidx.databinding.ObservableField
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.orders.model.OrderProductItem
import grand.app.alamghareeb.utils.resources.ResourceManager

class ItemOrderProductViewModel(var item: OrderProductItem) : BaseViewModel() {
    var obsPrice = ObservableField<String>()
    var name = StringBuilder()
    var obsItemName = ObservableField<String>()

    init {
        obsPrice.set("${item.total} ${ResourceManager.getString(R.string.label_currency)}")

        name.append(item.name)
        when {
            item.color != null && item.color != "" -> {
                name.append(" - " + item.color)
            }
        }
        when {
            item.size != null && item.size != "" -> {
                name.append(" - " + item.size)
            }
        }
        obsItemName.set(name.toString())
    }
}