package grand.app.alamghareeb.main.filter.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawFilterSubcategoryBinding
import grand.app.alamghareeb.main.filter.viewmodel.ItemFilterSubCategoryViewModel
import grand.app.alamghareeb.main.products.model.SubCategoriesItem
import grand.app.alamghareeb.utils.SingleLiveEvent
import grand.app.alamghareeb.utils.constants.Params
import java.util.*

class FilterSubCategoryAdapter : RecyclerView.Adapter<FilterSubCategoryAdapter.BranchesHolder>() {
    var itemsList: ArrayList<SubCategoriesItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<SubCategoriesItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BranchesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawFilterSubcategoryBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_filter_subcategory, parent, false)
        return BranchesHolder(binding)
    }

    override fun onBindViewHolder(holder: BranchesHolder, position: Int) {
        val itemViewModel = ItemFilterSubCategoryViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                selectedPosition != position -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
                else -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<SubCategoriesItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class BranchesHolder(val binding: RawFilterSubcategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_checked)
                }
                else -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_unchecked)
                }
            }
        }
    }
}
