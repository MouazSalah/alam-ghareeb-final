package grand.app.alamghareeb.main.restoreorder.model

class RestoreRequest {
    var reason_id: Int? = null
    var product_id: Int? = null
    var quantity: Int? = null
    var reason: String? = null
}