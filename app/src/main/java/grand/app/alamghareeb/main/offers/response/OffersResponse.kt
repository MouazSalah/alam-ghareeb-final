package grand.app.alamghareeb.main.offers.response

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.home.model.SliderItem

data class OffersResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val offersData: OffersData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class OffersData(

    @field:SerializedName("offers")
    val offersList: List<ProductItem?>? = null,

    @field:SerializedName("slider")
    val sliderList: List<SliderItem?>? = null
)

