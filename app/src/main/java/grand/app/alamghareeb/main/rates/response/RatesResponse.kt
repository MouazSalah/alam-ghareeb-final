package grand.app.alamghareeb.main.rates.response

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.orders.model.Links
import grand.app.alamghareeb.main.orders.model.Meta

data class RatesResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val ratesData: RatesData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class RateItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("image")
    val image: Any? = null,

    @field:SerializedName("rate")
    val rate: Int? = null,

    @field:SerializedName("review")
    val review: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class RatesData(

    @field:SerializedName("data")
    val ratesList: List<RateItem?>? = null,

    @field:SerializedName("meta")
    val meta: Meta? = null,

    @field:SerializedName("links")
    val links: Links? = null
)