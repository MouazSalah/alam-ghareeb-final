package grand.app.alamghareeb.main.offers.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.adapter.view.SlidersAdapter
import grand.app.alamghareeb.main.adapter.view.HorizentalProductsAdapter
import grand.app.alamghareeb.main.adapter.view.VerticalProductsAdapter
import grand.app.alamghareeb.main.favorites.model.AddToFavRequest
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.offers.response.OffersResponse
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class OffersViewModel @Inject constructor() : BaseViewModel() {
    var offersAdapter = VerticalProductsAdapter()
    var slidersAdapter = SlidersAdapter()
    var obsAddress = ObservableField<String>()

    fun onSearchClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.SEARCH_CLICKED)
    }

    fun getOffers() {

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.OFFERS) }) { res ->
            obsIsProgress.set(false)
            val response: OffersResponse = Gson().fromJson(res, OffersResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    when {
                        response.offersData?.sliderList!!.isNotEmpty() -> {
                            apiResponseLiveData.value = ApiResponse.success(response)
                        }
                    }
                    when {
                        response.offersData?.sliderList!!.isNotEmpty() -> {
                            offersAdapter.updateList(response.offersData.offersList as ArrayList<ProductItem>)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun addToFav(item: ProductItem) {
        val request = AddToFavRequest()
        request.product_id = item.id
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.FAVORITES, request) }) { res ->
            obsIsProgress.set(false)
            val response: AddToFavResponse = Gson().fromJson(res, AddToFavResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun addToCart(item: ProductItem) {

    }

    init {
        getOffers()
    }
}