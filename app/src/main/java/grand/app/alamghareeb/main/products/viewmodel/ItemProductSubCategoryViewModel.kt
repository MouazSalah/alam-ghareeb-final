package grand.app.alamghareeb.main.products.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.main.products.model.SubCategoriesItem
import grand.app.alamghareeb.main.products.model.SubCategoryProductsItem

class ItemProductSubCategoryViewModel(var item: SubCategoriesItem) : BaseViewModel()