package grand.app.alamghareeb.main.help.view

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import es.dmoral.toasty.Toasty
import grand.app.alamghareeb.main.help.viewmodel.HelpViewModel
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentHelpBinding
import grand.app.alamghareeb.dialogs.language.DialogLanguageFragment
import grand.app.alamghareeb.utils.LocalUtil
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class HelpFragment : BaseFragment() {
    lateinit var binding: FragmentHelpBinding
    lateinit var viewModel: HelpViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_help, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(HelpViewModel::class.java)
        binding.viewModel = viewModel

        observe(viewModel.adapter.itemLiveData) {
            it.let {
                when (it?.id) {
                    0 -> {
                        // rate app
                        val packageName = "grand.diaaalsalheen"
                        val uri = Uri.parse("market://details?id=$packageName")
                        val myAppLinkToMarket = Intent(Intent.ACTION_VIEW, uri)
                        try {
                            startActivity(myAppLinkToMarket)
                        } catch (e: ActivityNotFoundException) {
                            Toasty.warning(requireActivity(),
                                "Impossible to find an application for the market").show()
                        }
                    }
                    1 -> {
                        Utils.shareApp(requireActivity(),
                            "تطبيق عالم غريب ",
                            "https://play.google.com/store/apps/details?id=com.attar.app")
                    }
                    2 -> {
                        findNavController().navigate(R.id.help_to_suggestions)
                    }
                    3 -> {
                        findNavController().navigate(HelpFragmentDirections.helpToAbout(Codes.TERMS))
                    }
                    4 -> {
                        findNavController().navigate(HelpFragmentDirections.helpToAbout(Codes.PRIVACY_POLICY))
                    }
                    5 -> {
                        findNavController().navigate(R.id.help_to_contact_us)
                    }
                    6 -> {
                        findNavController().navigate(HelpFragmentDirections.helpToAbout(Codes.ABOUT_APP))
                    }
                    7 -> {
                        Utils.startDialogActivity(requireActivity(),
                            DialogLanguageFragment::class.java.name,
                            Codes.DIALOG_SELECT_LANGUAGE,
                            null)
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.DIALOG_SELECT_LANGUAGE -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        val lang = data.getStringExtra(Params.LANGUAGE)
                                        Timber.e("mou3az_language : " + lang.toString())
                                        LocalUtil.changeLang(requireActivity(), lang.toString())
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}