package grand.app.alamghareeb.main.products.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.home.model.CategoriesItem

class ItemProductCategoryViewModel(var item: CategoriesItem) : BaseViewModel()