package grand.app.alamghareeb.main.confirmorder.model

import com.google.gson.annotations.SerializedName

data class ConfirmOrderResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: Any? = null,

    @field:SerializedName("message")
    val message: String? = null
)
