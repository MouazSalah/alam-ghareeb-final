package grand.app.alamghareeb.main.productdetails.view

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawProductColorBinding
import grand.app.alamghareeb.main.productdetails.model.ProductColorItem
import grand.app.alamghareeb.main.productdetails.viewmodel.ItemColorViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import grand.app.alamghareeb.utils.resources.ResourceManager
import timber.log.Timber
import java.util.*

class ProductColorsAdapter : RecyclerView.Adapter<ProductColorsAdapter.ProductColorsHolder>() {
    var itemsList: ArrayList<ProductColorItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<ProductColorItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductColorsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawProductColorBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_product_color, parent, false)
        return ProductColorsHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductColorsHolder, position: Int) {
        val itemViewModel = ItemColorViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.fabBtn.backgroundTintList =
            ColorStateList.valueOf(Color.parseColor(itemViewModel.item.value));

        holder.setSelected()
        holder.binding.fabBtn.setOnClickListener {
            when {
                selectedPosition != position -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                }
                else -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                }
            }
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ProductColorItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class ProductColorsHolder(val binding: RawProductColorBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.fabBtn.setImageResource(R.drawable.ic_color_check)
                }
                else -> {
                    binding.fabBtn.setImageDrawable(null)
                }
            }
        }
    }
}
