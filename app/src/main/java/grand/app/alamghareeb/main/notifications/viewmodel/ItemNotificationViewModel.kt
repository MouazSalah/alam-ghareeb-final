package grand.app.alamghareeb.main.notifications.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.notifications.model.NotificationItem

class ItemNotificationViewModel(var item: NotificationItem) : BaseViewModel()