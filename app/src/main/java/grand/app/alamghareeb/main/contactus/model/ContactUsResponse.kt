package grand.app.alamghareeb.main.contactus.model

import com.google.gson.annotations.SerializedName

data class ContactUsResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val contactUsList: List<ContactUsItem?>? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class ContactUsItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: String? = null
)
