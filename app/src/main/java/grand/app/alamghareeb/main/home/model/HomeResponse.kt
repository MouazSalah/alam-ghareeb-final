package grand.app.alamghareeb.main.home.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class HomeResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val homeData: HomeData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class HomeData(

    @field:SerializedName("offers")
    val offersList: List<ProductItem?>? = null,

    @field:SerializedName("slider")
    val slidersList: List<SliderItem?>? = null,

    @field:SerializedName("top_rated")
    val topRatedList: List<ProductItem?>? = null,

    @field:SerializedName("banner")
    val bannerList: List<SliderItem?>? = null,

    @field:SerializedName("categories")
    val categoriesList: List<CategoriesItem?>? = null
)

data class SliderItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("category_id")
    val categoryId: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("sort")
    val sort: Int? = null
)

data class CategoriesItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
) : Serializable

data class ProductItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("price_before")
    val priceBefore: Int? = null,

    @field:SerializedName("is_favorite")
    var isFavorite: Boolean? = null,

    @field:SerializedName("rate")
    val rate: Float? = null,

    @field:SerializedName("price")
    val price: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

