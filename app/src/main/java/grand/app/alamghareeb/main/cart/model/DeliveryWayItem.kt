package grand.app.alamghareeb.main.cart.model

data class DeliveryWayItem(
    val id: Int? = null,
    val name: String? = null,
    var list: ArrayList<BranchItem>? = arrayListOf()
)
