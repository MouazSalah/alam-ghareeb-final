package grand.app.alamghareeb.main.confirmorder.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawOrderReviewBinding
import grand.app.alamghareeb.main.cart.model.CartItem
import grand.app.alamghareeb.main.confirmorder.viewmodel.ItemOrderReviewViewModel
import java.util.*

class OrderReviewAdapter : RecyclerView.Adapter<OrderReviewAdapter.CartsHolder>() {
    var itemsList: ArrayList<CartItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawOrderReviewBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_order_review, parent, false)
        return CartsHolder(binding)
    }

    override fun onBindViewHolder(holder: CartsHolder, position: Int) {
        val itemViewModel = ItemOrderReviewViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<CartItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class CartsHolder(val binding: RawOrderReviewBinding) :
        RecyclerView.ViewHolder(binding.root)
}
