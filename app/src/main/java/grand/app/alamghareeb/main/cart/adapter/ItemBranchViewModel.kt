package grand.app.alamghareeb.main.cart.adapter

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.cart.model.BranchItem

class ItemBranchViewModel(var item: BranchItem) : BaseViewModel()