package grand.app.alamghareeb.main.orderdetails.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.orders.model.HistoryItem
import grand.app.alamghareeb.main.orders.model.OrderProductItem
import java.io.Serializable

data class OrderDetailsResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val orderDetailsData: OrderDetailsData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class ProductsItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("total")
    val total: Int? = null,

    @field:SerializedName("quantity")
    val quantity: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class OrderDetailsData(

    @field:SerializedName("addresses")
    val addresses: String? = null,

    @field:SerializedName("discount")
    val discount: Int? = null,

    @field:SerializedName("history")
    val historyList: List<HistoryItem?>? = null,

    @field:SerializedName("products")
    val productsList: List<OrderProductItem?>? = null,

    @field:SerializedName("canceled")
    val canceled: Int? = null,

    @field:SerializedName("total")
    val total: Int? = null,

    @field:SerializedName("delivery_fees")
    val deliveryFees: Int? = null,

    @field:SerializedName("phone")
    val phone: String? = null,

    @field:SerializedName("subtotal")
    val subtotal: Int? = null,

    @field:SerializedName("branch_name")
    val branchName: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("pay_type")
    val payType: String? = null,

    @field:SerializedName("branch_latitude")
    val branchLatitude: String? = null,

    @field:SerializedName("branch_longitude")
    val branchLongitude: String? = null,

    @field:SerializedName("payment_method")
    val paymentMethod: String? = null
) : Serializable