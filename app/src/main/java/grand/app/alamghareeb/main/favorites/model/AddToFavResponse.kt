package grand.app.alamghareeb.main.favorites.response

import com.google.gson.annotations.SerializedName

data class AddToFavResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: Data? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class Data(

    @field:SerializedName("is_favorite")
    val isFavorite: Boolean? = null
)
