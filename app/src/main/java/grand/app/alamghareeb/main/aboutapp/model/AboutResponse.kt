package grand.app.alamghareeb.main.aboutapp.model

import com.google.gson.annotations.SerializedName

data class AboutResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val aboutData: AboutData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class AboutData(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: String? = null
)
