package grand.app.alamghareeb.main.confirmorder.model

data class OrderPaymentItem(
    var id: Int? = null,
    var name: String? = null
)
