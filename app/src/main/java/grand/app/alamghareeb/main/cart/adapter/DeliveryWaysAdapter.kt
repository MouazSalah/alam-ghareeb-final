package grand.app.alamghareeb.main.cart.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawDeliveryWayBinding
import grand.app.alamghareeb.main.cart.model.DeliveryWayItem
import grand.app.alamghareeb.utils.SingleLiveEvent
import timber.log.Timber
import java.util.*

class DeliveryWaysAdapter : RecyclerView.Adapter<DeliveryWaysAdapter.SalonCategoriesHolder>() {
    var itemsList: ArrayList<DeliveryWayItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<DeliveryWayItem>()
    var selectedPosition = -1
    var branchId: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SalonCategoriesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawDeliveryWayBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_delivery_way, parent, false)
        return SalonCategoriesHolder(binding)
    }

    override fun onBindViewHolder(holder: SalonCategoriesHolder, position: Int) {
        val itemViewModel = ItemDeliveryWayViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        when (itemViewModel.item.id) {
            0 -> {
                holder.binding.icArrow.visibility = View.INVISIBLE
            }
            else -> {
                holder.binding.icArrow.visibility = View.VISIBLE
            }
        }

        itemViewModel.adapter.itemLiveData.observeForever {
            when {
                itemViewModel.adapter.selectedPosition != -1 -> {
                    branchId = it?.id!!
                }
                else -> {
                    Timber.e("branch_item : -1 ")
                    branchId = -1
                }
            }
        }

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                selectedPosition != position -> {
                    itemLiveData.value = itemViewModel.item
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                }
                else -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<DeliveryWayItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class SalonCategoriesHolder(val binding: RawDeliveryWayBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.ivSelected.setImageResource(R.drawable.ic_branch_checked)
                    when (itemsList[selectedPosition].id) {
                        1 -> {
                            binding.rvBranches.visibility = View.VISIBLE
                        }
                        else -> {
                            binding.rvBranches.visibility = View.GONE
                        }
                    }
                }
                else -> {
                    binding.ivSelected.setImageResource(R.drawable.ic_branch_unchecked)
                    binding.rvBranches.visibility = View.GONE
                }
            }
        }
    }
}
