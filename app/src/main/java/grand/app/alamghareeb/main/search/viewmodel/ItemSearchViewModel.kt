package grand.app.alamghareeb.main.search.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.home.model.CategoriesItem

class ItemSearchViewModel(var item: CategoriesItem) : BaseViewModel()