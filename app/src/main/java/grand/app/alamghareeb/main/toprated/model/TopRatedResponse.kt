package grand.app.alamghareeb.main.toprated.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.orders.model.Links
import grand.app.alamghareeb.main.orders.model.Meta

data class TopRatedResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val topRatedData: TopRatedData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class TopRatedData(

    @field:SerializedName("data")
    val productsList: List<ProductItem?>? = null,

    @field:SerializedName("meta")
    val meta: Meta? = null,

    @field:SerializedName("links")
    val links: Links? = null
)
