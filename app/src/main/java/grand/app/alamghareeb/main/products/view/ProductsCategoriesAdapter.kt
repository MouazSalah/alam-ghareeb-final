package grand.app.alamghareeb.main.products.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawProductCategoryBinding
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.main.products.viewmodel.ItemProductCategoryViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import timber.log.Timber
import java.util.*

class ProductsCategoriesAdapter :
    RecyclerView.Adapter<ProductsCategoriesAdapter.ProductsCategoriesHolder>() {
    var itemsList: ArrayList<CategoriesItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<CategoriesItem>()
    var selectedPosition = -1
    var categoryId : Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsCategoriesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawProductCategoryBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_product_category, parent, false)
        return ProductsCategoriesHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductsCategoriesHolder, position: Int) {
        val itemViewModel = ItemProductCategoryViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        when (itemViewModel.item.id) {
            categoryId -> {
                selectedPosition = position
            }
        }

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                selectedPosition != position -> {
                    categoryId = itemViewModel.item.id!!
                    itemLiveData.value = itemViewModel.item
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<CategoriesItem>, catId : Int) {
        categoryId = catId
        Timber.e("mou3az_adapter : categories " + models.size)
        itemsList = models
        notifyDataSetChanged()
    }

    inner class ProductsCategoriesHolder(val binding: RawProductCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.icChecked.visibility = View.VISIBLE
                }
                else -> {
                    binding.icChecked.visibility = View.GONE
                }
            }
        }
    }
}
