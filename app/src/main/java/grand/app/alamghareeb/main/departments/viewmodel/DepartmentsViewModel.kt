package grand.app.alamghareeb.main.departments.viewmodel

import androidx.databinding.ObservableField
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.adapter.view.CategoriesAdapter
import grand.app.alamghareeb.main.adapter.view.SlidersAdapter
import grand.app.alamghareeb.utils.constants.Codes

class DepartmentsViewModel : BaseViewModel() {
    var bannersAdapter = SlidersAdapter()
    var categoriesAdapter = CategoriesAdapter()
    var obsSearchNam = ObservableField<String>()

    fun onSearchClicked() {
        when {
            obsSearchNam.get() == null -> {
                setValue(Codes.EMPTY_SALON_NAME)
            }
            else -> {
                setValue(Codes.SEARCH_CLICKED)
            }
        }
    }

    fun getHomePage() {
//        obsLayout.set(LoadingStatus.SHIMMER)
//        requestCall<HomeResponse?>({
//            withContext(Dispatchers.IO) { return@withContext getApiRepo().getHome() }
//        })
//        { res ->
//            when (res!!.code) {
//                200 -> {
//                    obsLayout.set(LoadingStatus.FULL)
//                    apiResponseLiveData.value = ApiResponse.success(res)
//                    categoriesAdapter.updateList(res.homeData?.homeCategoriesList as ArrayList<CategoriesItem>)
//                }
//                else -> {
//                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
//                }
//            }
//        }
    }

    fun updateToken(token: String) {
//        requestCall<UpdateTokenResponse?>({
//            withContext(Dispatchers.IO) { return@withContext getApiRepo().updateToken(UpdateTokenRequest(token)) }
//        })
//        { res ->
//            when (res!!.code) {
//                200 -> {
//
//                }
//                401 -> {
//                    apiResponseLiveData.value = ApiResponse.errorMessage(res.msg)
//                }
//            }
//        }
    }
}