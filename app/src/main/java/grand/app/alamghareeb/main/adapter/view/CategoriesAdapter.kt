package grand.app.alamghareeb.main.adapter.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawCategoryBinding
import grand.app.alamghareeb.main.adapter.viewmodel.ItemCategoryViewModel
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.utils.SingleLiveEvent
import java.util.*

class CategoriesAdapter : RecyclerView.Adapter<CategoriesAdapter.AboutHolder>() {
    var itemsList: ArrayList<CategoriesItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<CategoriesItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AboutHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawCategoryBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_category, parent, false)
        return AboutHolder(binding)
    }

    override fun onBindViewHolder(holder: AboutHolder, position: Int) {
        val itemViewModel = ItemCategoryViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<CategoriesItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class AboutHolder(val binding: RawCategoryBinding) : RecyclerView.ViewHolder(binding.root)
}
