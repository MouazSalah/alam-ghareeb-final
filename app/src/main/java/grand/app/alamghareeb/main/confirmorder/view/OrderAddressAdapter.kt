package grand.app.alamghareeb.main.confirmorder.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawOrderAddressBinding
import grand.app.alamghareeb.main.addresses.model.ListAddressItem
import grand.app.alamghareeb.main.confirmorder.viewmodel.ItemOrderAddressViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class OrderAddressAdapter : RecyclerView.Adapter<OrderAddressAdapter.OrderAddressHolder>() {
    var itemsList = ArrayList<ListAddressItem>()
    var itemLiveData = SingleLiveEvent<ListAddressItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderAddressHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawOrderAddressBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_order_address, parent, false)
        return OrderAddressHolder(binding)
    }

    override fun onBindViewHolder(holder: OrderAddressHolder, position: Int) {
        val itemViewModel = ItemOrderAddressViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.ibCheck.setOnClickListener {
            when {
                selectedPosition != position -> {
                    itemLiveData.value = itemViewModel.item
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                }
                else -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ListAddressItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    fun getItem(pos: Int): ListAddressItem {
        return itemsList[pos]
    }

    inner class OrderAddressHolder(val binding: RawOrderAddressBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_checked)
                }
                else -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_unchecked)
                }
            }
        }
    }
}
