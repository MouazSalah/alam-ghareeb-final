package grand.app.alamghareeb.main.groups.base

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.ogaclejapan.smarttablayout.utils.v4.FragmentStatePagerItemAdapter
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentGroupsBinding
import grand.app.alamghareeb.main.cart.model.CartItem
import grand.app.alamghareeb.main.groups.mostselling.view.MostSellingFragment
import grand.app.alamghareeb.main.groups.oldorders.view.OldOrdersFragment
import grand.app.alamghareeb.main.groups.savingoffers.view.SavingOffersFragment
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import java.util.ArrayList

class GroupsFragment : BaseFragment() {

    private lateinit var binding: FragmentGroupsBinding
    val fragmentArgs: GroupsFragmentArgs by navArgs()

    private val navController by lazy {
        activity.let {
            Navigation.findNavController(it!!, R.id.nav_home_host_fragment)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_groups, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

        binding.ibBack.setOnClickListener {
            findNavController().navigate(R.id.navigation_home)
        }

        binding.ibSearch.setOnClickListener {
            findNavController().navigate(R.id.groups_to_search)
        }
    }

    private fun init() {

        val adapter = FragmentStatePagerItemAdapter(
            childFragmentManager, FragmentPagerItems.with(requireActivity())
                .add(R.string.title_previos_orders, OldOrdersFragment::class.java)
                .add(R.string.title_saving_offers, SavingOffersFragment::class.java)
                .add(R.string.title_most_selling, MostSellingFragment::class.java)
                .create()
        )

        val viewPager = binding.pager
        binding.pager.adapter = adapter

        viewPager.currentItem = fragmentArgs.flag

        val viewPagerTab = binding.tabLayout
        viewPagerTab.setViewPager(viewPager)

        when (PrefMethods.getUserCarts()?.size) {
            0 -> {
                binding.cartLayout.visibility = View.GONE
            }
            else -> {
                binding.cartLayout.visibility = View.VISIBLE
                var totalPrice = 0.0
                val cartsList: ArrayList<CartItem>? = PrefMethods.getUserCarts()
                when {
                    cartsList != null -> {
                        for (i in cartsList) {
                            totalPrice += i.total!!
                        }
                    }
                }
                binding.tvCartCount.text = cartsList?.size.toString()
                binding.tvCartPrice.text = "$totalPrice ${getString(R.string.label_currency)}"

                binding.cartLayout.setOnClickListener {
                    // navController.navigate(R.id.groups_to_carts)
                    navigate(R.id.navigation_carts)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(),
                                            AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN,
                                            true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (Const.isAskedToLogin == 1 && PrefMethods.getUserData() != null) {
            Const.isAskedToLogin = 0
            // viewModel.addToFav(branchItem.id!!)
            init()
        }
    }
}