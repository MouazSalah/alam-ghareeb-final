package grand.app.alamghareeb.main.filter.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.productdetails.model.ProductColorItem
import grand.app.alamghareeb.main.products.model.SubCategoriesItem

data class FilterDetailsResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val filterDetailsData: FilterDetailsData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class FilterDetailsData(

    @field:SerializedName("sub_categories")
    val subCategoriesList: List<SubCategoriesItem>? = null,

    @field:SerializedName("brands")
    val brandsList: List<String>? = null,

    @field:SerializedName("max_price")
    val maxPrice: Int? = null,

    @field:SerializedName("min_price")
    val minPrice: Int? = null,

    @field:SerializedName("attributes")
    val colorsList: List<ProductColorItem?>? = null
)

data class FilterBrandItem(
    val brand: String? = null
)


