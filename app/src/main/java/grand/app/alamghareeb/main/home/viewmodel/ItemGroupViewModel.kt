package grand.app.alamghareeb.main.home.viewmodel

import android.graphics.drawable.Drawable

data class ItemGroupViewModel(
    var id: Int? = null,
    var title: String? = null,
    var image: Drawable? = null
)
