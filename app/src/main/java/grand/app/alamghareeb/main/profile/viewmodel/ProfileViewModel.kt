package grand.app.alamghareeb.main.profile.viewmodel

import androidx.core.content.ContextCompat
import androidx.databinding.ObservableField
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.help.view.ItemHelpModel
import grand.app.alamghareeb.main.profile.view.ProfileAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.utils.PrefMethods
import timber.log.Timber

class ProfileViewModel : BaseViewModel() {
    var adapter = ProfileAdapter()
    var obsName = ObservableField<String>()

    init {
        when {
            PrefMethods.getUserData() != null -> {
                obsName.set(PrefMethods.getUserData()?.name.toString())
            }
            else -> {
                obsName.set(getString(R.string.label_visitor))
            }
        }

        val profileList = arrayListOf(
            ItemHelpModel(0,
                getString(R.string.title_addresses),
                ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_addresses)),
            ItemHelpModel(1,
                getString(R.string.title_my_bookings),
                ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_my_orders)),
            ItemHelpModel(2,
                getString(R.string.title_wallet),
                ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_wallet)),
            ItemHelpModel(3,
                getString(R.string.title_favorite),
                ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_favorites)),
            ItemHelpModel(4,
                getString(R.string.title_help),
                ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_help)),
            getLastItem()
        )

        adapter.updateList(profileList)

        notifyChange()
    }

    private fun getLastItem(): ItemHelpModel {
        return when {
            PrefMethods.getUserData() == null -> {
                ItemHelpModel(5,
                    getString(R.string.title_login),
                    ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_login))
            }
            else -> {
                ItemHelpModel(5,
                    getString(R.string.title_logout),
                    ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_login))
            }
        }
    }

    fun onProfileClicked(value: Int) {
        setValue(value)
    }
}