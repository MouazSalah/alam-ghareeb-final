package grand.app.alamghareeb.main.help.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawHelpBinding
import grand.app.alamghareeb.main.help.viewmodel.ItemHelpViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import java.util.*

class HelpAdapter : RecyclerView.Adapter<HelpAdapter.MoreProfileHolder>() {
    var itemsList: ArrayList<ItemHelpModel> = ArrayList()
    var itemLiveData = SingleLiveEvent<ItemHelpModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoreProfileHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawHelpBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_help, parent, false)
        return MoreProfileHolder(binding)
    }

    override fun onBindViewHolder(holder: MoreProfileHolder, position: Int) {
        val itemViewModel = ItemHelpViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ItemHelpModel>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class MoreProfileHolder(val binding: RawHelpBinding) :
        RecyclerView.ViewHolder(binding.root)
}
