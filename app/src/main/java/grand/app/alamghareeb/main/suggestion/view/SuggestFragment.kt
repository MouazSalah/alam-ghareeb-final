package grand.app.alamghareeb.main.suggestion.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import grand.app.alamghareeb.main.suggestion.viewmodel.SuggestViewModel
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentSuggestBinding
import grand.app.alamghareeb.main.suggestion.model.SuggestResponse
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class SuggestFragment : BaseFragment() {
    lateinit var binding: FragmentSuggestBinding

    @Inject
    lateinit var viewModel: SuggestViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_suggest, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is SuggestResponse -> {
                            showToast(it.data.msg.toString(), 2)
                            findNavController().navigateUp()
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}