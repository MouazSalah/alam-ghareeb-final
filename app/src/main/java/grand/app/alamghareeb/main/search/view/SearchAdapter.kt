package grand.app.alamghareeb.main.search.view

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawSearchBinding
import grand.app.alamghareeb.main.adapter.viewmodel.ItemCategoryViewModel
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.utils.SingleLiveEvent
import java.util.*
import kotlin.collections.ArrayList

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.AboutHolder>(), Filterable {
    var allItemsList: ArrayList<CategoriesItem> = ArrayList()
    var itemsList: ArrayList<CategoriesItem> = ArrayList()
    var filteredList: ArrayList<CategoriesItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<CategoriesItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AboutHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawSearchBinding = DataBindingUtil.inflate(layoutInflater,
            R.layout.raw_search,
            parent,
            false)
        return AboutHolder(binding)
    }

    override fun onBindViewHolder(holder: AboutHolder, position: Int) {
        val itemViewModel = ItemCategoryViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<CategoriesItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class AboutHolder(val binding: RawSearchBinding) : RecyclerView.ViewHolder(binding.root)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    filteredList = allItemsList
                } else {
                    val resultList = ArrayList<CategoriesItem>()
                    for (row in allItemsList) {
                        if (row.name?.toLowerCase(Locale.ROOT)!!
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {
                            resultList.add(row)
                        }
                    }
                    filteredList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = filteredList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                updateList(results?.values as ArrayList<CategoriesItem>)
            }
        }
    }
}



