package grand.app.alamghareeb.main.groups.oldorders.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.adapter.view.VerticalProductsAdapter
import grand.app.alamghareeb.main.favorites.model.AddToFavRequest
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.toprated.model.TopRatedResponse
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import timber.log.Timber
import javax.inject.Inject

open class OldOrdersViewModel @Inject constructor() : BaseViewModel() {
    var adapter = VerticalProductsAdapter()

    fun getProducts() {

        when {
            PrefMethods.getUserData() == null -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
            else -> {
                obsLayout.set(LoadingStatus.SHIMMER)
                requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.ORDERED_BEFORE) }) { res ->
                    obsIsProgress.set(false)
                    val response: TopRatedResponse = Gson().fromJson(res, TopRatedResponse::class.java)
                    when (response.code) {
                        200 -> {
                            when {
                                response.topRatedData?.productsList!!.isNotEmpty() -> {
                                    adapter.updateList(response.topRatedData.productsList as ArrayList<ProductItem>)
                                    obsLayout.set(LoadingStatus.FULL)
                                }
                                else -> {
                                    obsLayout.set(LoadingStatus.EMPTY)
                                }
                            }
                        }
                        else -> {
                            apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                        }
                    }
                }
            }
        }
    }

    fun addToFav(item: ProductItem) {
        val request = AddToFavRequest()
        request.product_id = item.id
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.FAVORITES, request) }) { res ->
            obsIsProgress.set(false)
            val response: AddToFavResponse = Gson().fromJson(res, AddToFavResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onLoginClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getProducts()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}