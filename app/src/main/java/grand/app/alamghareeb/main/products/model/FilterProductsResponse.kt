package grand.app.alamghareeb.main.products.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.orders.model.Links
import grand.app.alamghareeb.main.orders.model.Meta

data class FilterProductsResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val filterProductsData: FilterProductsData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class FilterProductsData(

    @field:SerializedName("data")
    val productsList: List<ProductItem?>? = null,

    @field:SerializedName("meta")
    val meta: Meta? = null,

    @field:SerializedName("links")
    val links: Links? = null
)