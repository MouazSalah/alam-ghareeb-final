package grand.app.alamghareeb.main.wallet.viewmodel

import androidx.databinding.ObservableField
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.wallet.model.WalletProductItem

class ItemWalletViewModel(var item: WalletProductItem) : BaseViewModel() {
    var obsPrice = ObservableField<String>()

    init {
        obsPrice.set("${item.price} ${getString(R.string.label_currency)}")
    }
}