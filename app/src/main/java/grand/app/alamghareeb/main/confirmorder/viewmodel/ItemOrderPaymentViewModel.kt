package grand.app.alamghareeb.main.confirmorder.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.confirmorder.model.OrderPaymentItem

class ItemOrderPaymentViewModel(var item: OrderPaymentItem) : BaseViewModel()
