package grand.app.alamghareeb.main.filter.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.ActivityFilterBinding
import grand.app.alamghareeb.dialogs.toast.DialogToastFragment
import grand.app.alamghareeb.main.filter.model.FilterDetailsResponse
import grand.app.alamghareeb.main.filter.model.FilterRequest
import grand.app.alamghareeb.main.filter.viewmodel.FilterViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class FilterActivity : AppCompatActivity() {

    lateinit var binding: ActivityFilterBinding
    lateinit var viewModel: FilterViewModel
    var isShowSub: Boolean = false
    private var isShowbrands: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_filter)
        viewModel = ViewModelProvider(this).get(FilterViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.getFilterDetails()

        binding.subcategoryLayout.setOnClickListener {
            when {
                !isShowSub -> {
                    binding.rvSubCategories.visibility = View.GONE
                    isShowSub = true
                }
                else -> {
                    binding.rvSubCategories.visibility = View.VISIBLE
                    isShowSub = false
                }
            }
        }

        binding.brandsLayout.setOnClickListener {
            when {
                !isShowbrands -> {
                    binding.rvBrands.visibility = View.GONE
                    isShowbrands = true
                }
                else -> {
                    binding.rvBrands.visibility = View.VISIBLE
                    isShowbrands = false
                }
            }
        }

        observe(viewModel.brandsAdapter.itemLiveData) {
            viewModel.brandsList?.add(it?.brand!!)
            Params.brands.add(it?.brand!!)
        }

        observe(viewModel.subAdapter.itemLiveData) {
            viewModel.subCatId = it?.id

            Params.sub_category_ids.clear()
            Params.sub_category_ids.add(it?.id!!)
        }

        observe(viewModel.colorsAdapter.itemLiveData) {
            viewModel.filterRequest.parent_id = it?.id!!
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is FilterDetailsResponse -> {
                            binding.priceSeekbar.setMinValue(viewModel.obsMinPrice.get()
                                ?.toFloat()!!)
                                .setMaxValue(viewModel.obsMaxPrice.get()?.toFloat()!!).apply();
                            viewModel.isPriceChanged = 2

                            if (it.data.filterDetailsData?.brandsList.isNullOrEmpty()) {
                                binding.brandsLayout.visibility = View.GONE
                                binding.rvBrands.visibility = View.GONE
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }

        when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> {
                val window: Window = window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.color_primary)
            }
        }

        binding.priceSeekbar.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            binding.tvMinPrice.text = "$minValue ريال "
            binding.tvMaxPrice.text = "$maxValue ريال "
            viewModel.isPriceChanged = 1
            viewModel.obsMinPrice.set(minValue.toInt())
            viewModel.obsMaxPrice.set(maxValue.toInt())
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.BACK_PRESSED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    setResult(Codes.FILTER_REQUEST, intent)
                    finish()
                }
                Codes.SEARCH_CLICKED -> {
                    val request = FilterRequest()
                    request.min_price = viewModel.obsMinPrice.get()
                    request.max_price = viewModel.obsMaxPrice.get()
                    request.sub_category_ids?.add(viewModel.subCatId!!)
                    request.brands = viewModel.brandsList

                    when (viewModel.isPriceChanged) {
                        1 -> {
                            viewModel.filterRequest.min_price = viewModel.obsMinPrice.get()
                            viewModel.filterRequest.max_price = viewModel.obsMaxPrice.get()
                            Params.min_price = viewModel.obsMinPrice.get()
                            Params.max_price = viewModel.obsMaxPrice.get()
                        }
                    }
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
                    intent.putExtra(Params.FILTER_REQUEST, request)
                    setResult(Codes.FILTER_REQUEST, intent)
                    finish()
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
        setResult(Codes.FILTER_REQUEST, intent)
        finish()
    }

    fun showToast(msg: String, type: Int) {
        val bundle = Bundle()
        bundle.putString(Params.DIALOG_TOAST_MESSAGE, msg)
        bundle.putInt(Params.DIALOG_TOAST_TYPE, type)
        Utils.startDialogActivity(this,
            DialogToastFragment::class.java.name,
            Codes.DIALOG_TOAST_REQUEST,
            bundle)
    }
}
