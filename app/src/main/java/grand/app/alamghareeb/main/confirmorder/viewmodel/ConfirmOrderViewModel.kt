package grand.app.alamghareeb.main.confirmorder.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.addresses.model.ListAddressItem
import grand.app.alamghareeb.main.addresses.model.ListAddressesResponse
import grand.app.alamghareeb.main.confirmorder.model.ConfirmOrderResponse
import grand.app.alamghareeb.main.confirmorder.model.OrderPaymentItem
import grand.app.alamghareeb.main.confirmorder.model.OrderProductRequest
import grand.app.alamghareeb.main.confirmorder.model.OrderRequest
import grand.app.alamghareeb.main.confirmorder.view.OrderAddressAdapter
import grand.app.alamghareeb.main.confirmorder.view.OrderPaymentAdapter
import grand.app.alamghareeb.main.confirmorder.view.OrderReviewAdapter
import grand.app.alamghareeb.main.wallet.model.WalletResponse
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall

class ConfirmOrderViewModel : BaseViewModel() {
    var orderRequest = OrderRequest()
    var addressesAdapter = OrderAddressAdapter()
    var paymentAdapter = OrderPaymentAdapter()
    var itemsAdapter = OrderReviewAdapter()

    var obsProductsCost = ObservableField<String>()
    var obsDeliveryCost = ObservableField<String>()
    var obsAfterDiscountCost = ObservableField<String>()
    var obsTotalPrice = ObservableField<String>()

    var productsCost: Double = 0.0
    var deliveryCost: Double = 0.0
    var totalAfterDiscount: Double = 0.0
    var totalCost: Double = 0.0
    var freeDeliveryCost: Double = 0.0

    var obsCurrentCredit = ObservableField<String>()
    var isWallet: Int = 0
    var branchId: Int = 0
    var walletCredit: Double = 0.0

    var obsDiscountValue = ObservableField<String>()

    fun onAddAddressClicked() {
        setValue(Codes.ADD_ADDRESS_CLICKED)
    }

    fun onApplyCouponClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.APPLY_COUPON_CLICKED)
    }

    fun onConfirmClicked() {
        when {
            branchId == -1 && addressesAdapter.selectedPosition == -1 -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.label_select_delivery_address))
            }
            paymentAdapter.selectedPosition == -1 -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.label_select_payment_way))
            }
            else -> {
                for (i in PrefMethods.getUserCarts()!!) {
                    val product = OrderProductRequest()
                    product.product_id = i.id
                    product.name = i.name
                    product.quantity = i.quantity
                    product.price = i.price
                    product.products_total = i.total
                    orderRequest.products.add(product)
                }

                orderRequest.payment_method = "cash"
                orderRequest.subtotal = productsCost
                orderRequest.delivery_fees = deliveryCost
                orderRequest.total = totalCost
                orderRequest.subtotal = totalAfterDiscount
                orderRequest.description = "extra notes"

                when {
                    isWallet != 0 -> {
                        orderRequest.is_wallet = 1
                        when {
                            walletCredit < totalCost -> {
                                orderRequest.wallet_amount = walletCredit
                            }
                            else -> {
                                orderRequest.wallet_amount = walletCredit - totalCost
                            }
                        }
                    }
                    else -> {
                        walletCredit = 0.0
                    }
                }

                obsIsProgress.set(true)
                requestCall<JsonObject?>({
                    getApiRepo().requestPostBody(URLS.CONFIRM_ORDER,
                        orderRequest)
                }) { res ->
                    obsIsProgress.set(false)
                    val response: ConfirmOrderResponse =
                        Gson().fromJson(res, ConfirmOrderResponse::class.java)
                    branchId = -1
                    when (response.code) {
                        200 -> {
                            PrefMethods.deleteUserCarts()
                            apiResponseLiveData.value = ApiResponse.success(response)
                        }
                        else -> {
                            apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                        }
                    }
                }
            }
        }
    }

    private fun getMyWallet() {
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.WALLET) }) { res ->
            obsIsProgress.set(false)
            val response: WalletResponse = Gson().fromJson(res, WalletResponse::class.java)
            when (response.code) {
                200 -> {
                    walletCredit = response.walletData?.balance?.toDouble()!!
                    obsCurrentCredit.set("${response.walletData.balance} ${getString(R.string.label_currency)}")
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun getAllAddresses() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.ALL_ADDRESSES) }) { res ->
            obsIsProgress.set(false)
            val response: ListAddressesResponse =
                Gson().fromJson(res, ListAddressesResponse::class.java)
            when (response.code) {
                200 -> {
                    freeDeliveryCost = response.listAddressesData?.freeDelivery!!
                    calculateCosts()
                    when (response.listAddressesData.addressesList!!.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            addressesAdapter.updateList(response.listAddressesData.addressesList as ArrayList<ListAddressItem>)
                            notifyChange()
                            obsLayout.set(LoadingStatus.FULL)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }

        itemsAdapter.updateList(PrefMethods.getUserCarts()!!)
        val paymentList = arrayListOf(
            OrderPaymentItem(0, getString(R.string.label_cash)),
            OrderPaymentItem(1, getString(R.string.label_online))
        )
        paymentAdapter.updateList(paymentList)
    }

    init {
        getAllAddresses()
        getMyWallet()
    }

    private fun calculateCosts() {
        for (i in PrefMethods.getUserCarts()!!) {
            productsCost = productsCost.plus(i.total!!)
        }

        totalAfterDiscount = productsCost
        deliveryCost = 0.0
        totalCost = totalAfterDiscount + deliveryCost

        obsProductsCost.set("$productsCost ${getString(R.string.label_currency)}")
        obsAfterDiscountCost.set("$totalAfterDiscount ${getString(R.string.label_currency)}")

        val deliveryDesc = StringBuilder()
        deliveryDesc.append(getString(R.string.label_if_free_delivery))
        deliveryDesc.append(" " + freeDeliveryCost)
        deliveryDesc.append(" " + getString(R.string.label_currency))
        deliveryDesc.append(" " + getString(R.string.label_free_delivery_cost))
        obsDiscountValue.set(deliveryDesc.toString())

        when (branchId) {
            -1 -> {
                when {
                    productsCost > freeDeliveryCost -> {
                        obsDeliveryCost.set(getString(R.string.label_delivery_free))
                    }
                    else -> {
                        obsDeliveryCost.set("${deliveryCost} ${getString(R.string.label_currency)}")
                    }
                }
            }
            else -> {
                obsDeliveryCost.set(getString(R.string.label_delivery_free))
            }
        }

        obsTotalPrice.set("$totalCost ${getString(R.string.label_currency)}")
        notifyChange()
    }
}