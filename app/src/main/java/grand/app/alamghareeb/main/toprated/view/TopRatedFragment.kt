package grand.app.alamghareeb.main.toprated.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.activity.main.BaseHomeFragment
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentTopRatedBinding
import grand.app.alamghareeb.dialogs.login.DialogLoginFragment
import grand.app.alamghareeb.main.cart.model.CartItem
import grand.app.alamghareeb.main.favorites.model.FavoritesResponse
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.offers.view.OffersFragmentDirections
import grand.app.alamghareeb.main.toprated.viewmodel.TopRatedViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import org.koin.android.ext.android.bind
import timber.log.Timber
import java.util.ArrayList
import javax.inject.Inject

class TopRatedFragment : BaseHomeFragment() {

    private lateinit var binding: FragmentTopRatedBinding

    @Inject
    lateinit var viewModel: TopRatedViewModel
    var productItem = ProductItem()
    val fragmentArgs: TopRatedFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_top_rated, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getProducts(fragmentArgs.flag)
        }

        when (fragmentArgs.flag) {
            URLS.TOP_RATED -> {
                binding.tvTitle.text = getString(R.string.label_top_rated)
            }
            else -> {
                binding.tvTitle.text = getString(R.string.label_best_offers)
            }
        }

        when (PrefMethods.getUserCarts()?.size) {
            0 -> {
                binding.cartLayout.visibility = View.GONE
            }
            else -> {
                binding.cartLayout.visibility = View.VISIBLE
                var totalPrice = 0.0
                val cartsList: ArrayList<CartItem>? = PrefMethods.getUserCarts()
                when {
                    cartsList != null -> {
                        for (i in cartsList) {
                            totalPrice += i.total!!
                        }
                    }
                }
                binding.tvCartCount.text = cartsList?.size.toString()
                binding.tvCartPrice.text = "$totalPrice ${getString(R.string.label_currency)}"

                binding.cartLayout.setOnClickListener {
                    navigate(R.id.navigation_carts)
                }
            }
        }

        viewModel.getProducts(fragmentArgs.flag)

        binding.ibBack.setOnClickListener {
            findNavController().navigateUp()
        }

        observe(viewModel.adapter.cartLiveData) {
            it?.let { item ->
                when {
                    PrefMethods.getUserData() == null -> {
                        Utils.startDialogActivity(requireActivity(),
                            DialogLoginFragment::class.java.name,
                            Codes.DIALOG_LOGIN_REQUEST,
                            null)
                    }
                    else -> {
                        findNavController().navigate(TopRatedFragmentDirections.topRatedToProductDetails(
                            item.id!!))
                    }
                }
            }
        }

        observe(viewModel.adapter.favLiveData) {
            it?.let { item ->
                when {
                    PrefMethods.getUserData() == null -> {
                        Utils.startDialogActivity(requireActivity(),
                            DialogLoginFragment::class.java.name,
                            Codes.DIALOG_LOGIN_REQUEST,
                            null)
                    }
                    else -> {
                        productItem = item
                        viewModel.addToFav(item)
                    }
                }
            }
        }

        observe(viewModel.apiResponseLiveData) { it ->
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is FavoritesResponse -> {
                        }
                        is AddToFavResponse -> {
                            showToast(it.data.message.toString(), 2)
                            it.data.data?.isFavorite?.let { it1 ->
                                viewModel.adapter.notifyItemSelected(productItem,
                                    it1)
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(),
                                            AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN,
                                            true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when {
            Const.isAskedToLogin == 1 && PrefMethods.getUserData() != null -> {
                Const.isAskedToLogin = 0
                viewModel.getProducts(fragmentArgs.flag)
            }
        }
    }
}