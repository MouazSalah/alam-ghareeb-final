package grand.app.alamghareeb.main.favorites.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.adapter.view.HorizentalProductsAdapter
import grand.app.alamghareeb.main.adapter.view.VerticalProductsAdapter
import grand.app.alamghareeb.main.favorites.model.AddToFavRequest
import grand.app.alamghareeb.main.favorites.model.FavoritesResponse
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class FavoritesViewModel @Inject constructor() : BaseViewModel() {
    var adapter = VerticalProductsAdapter()

    fun getFavorites() {

        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.FAVORITES) }) { res ->
            obsIsProgress.set(false)
            val response: FavoritesResponse = Gson().fromJson(res, FavoritesResponse::class.java)
            when (response.code) {
                200 -> {
                    when {
                        response.favoritesList!!.isNotEmpty() -> {
                            adapter.updateList(response.favoritesList as ArrayList<ProductItem>)
                            obsLayout.set(LoadingStatus.FULL)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                    }
                }
                else -> {
                    obsLayout.set(LoadingStatus.EMPTY)
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun addToFav(item: ProductItem) {
        val request = AddToFavRequest()
        request.product_id = item.id
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.FAVORITES, request) }) { res ->
            obsIsProgress.set(false)
            val response: AddToFavResponse = Gson().fromJson(res, AddToFavResponse::class.java)
            when (response.code) {
                200 -> {
                    adapter.notifyItemSelected(item, response.data?.isFavorite!!)
                    adapter.removeItem(item)

                    when (adapter.itemsList.size) {
                        0 -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.FULL)
                        }
                    }

                    apiResponseLiveData.value = ApiResponse.successMessage(response.message)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onLoginClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.LOGIN_CLICKED)

    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getFavorites()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}