package grand.app.alamghareeb.main.payment.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentPaymentBinding
import grand.app.alamghareeb.main.payment.viewmodel.PaymentViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class PaymentFragment : BaseFragment() {
    lateinit var binding: FragmentPaymentBinding
    lateinit var viewModelMore: PaymentViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModelMore = ViewModelProvider(this).get(PaymentViewModel::class.java)
        binding.viewModel = viewModelMore

        observe(viewModelMore.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {

                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }
}