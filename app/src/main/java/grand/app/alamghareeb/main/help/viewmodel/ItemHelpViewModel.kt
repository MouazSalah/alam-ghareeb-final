package grand.app.alamghareeb.main.help.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.help.view.ItemHelpModel

class ItemHelpViewModel(var item: ItemHelpModel) : BaseViewModel()