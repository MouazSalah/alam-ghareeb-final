package grand.app.alamghareeb.main.cart.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawCartBranchBinding
import grand.app.alamghareeb.main.cart.model.BranchItem
import grand.app.alamghareeb.utils.SingleLiveEvent
import grand.app.alamghareeb.utils.constants.Params
import timber.log.Timber
import java.util.*

class BranchesAdapter : RecyclerView.Adapter<BranchesAdapter.BranchesHolder>() {
    var itemsList: ArrayList<BranchItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<BranchItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BranchesHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawCartBranchBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_cart_branch, parent, false)
        return BranchesHolder(binding)
    }

    override fun onBindViewHolder(holder: BranchesHolder, position: Int) {
        val itemViewModel = ItemBranchViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                selectedPosition != position -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    Timber.e("branch_item : " + itemViewModel.item.id)
                    itemLiveData.value = itemViewModel.item
                }
                else -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<BranchItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class BranchesHolder(val binding: RawCartBranchBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_checked)
                }
                else -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_unchecked)
                }
            }
        }
    }
}
