package grand.app.alamghareeb.main.categories.model

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.main.home.model.SliderItem

data class CategoriesResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val categoriesData: CategoriesData? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class CategoriesData(

    @field:SerializedName("slider")
    val slidersList: List<SliderItem?>? = null,

    @field:SerializedName("categories")
    val categoriesList: List<CategoriesItem?>? = null
)
