package grand.app.alamghareeb.main.filter.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawFilterBrandBinding
import grand.app.alamghareeb.main.filter.model.FilterBrandItem
import grand.app.alamghareeb.main.filter.viewmodel.ItemFilterBrandViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import java.util.*

class FilterBrandAdapter : RecyclerView.Adapter<FilterBrandAdapter.FilterHolder>() {
    var itemsList: ArrayList<FilterBrandItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<FilterBrandItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawFilterBrandBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_filter_brand, parent, false)
        return FilterHolder(binding)
    }

    override fun onBindViewHolder(holder: FilterHolder, position: Int) {
        val itemViewModel = ItemFilterBrandViewModel(itemsList.get(position))
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                selectedPosition != position -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
                else -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<FilterBrandItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class FilterHolder(val binding: RawFilterBrandBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_checked)
                }
                else -> {
                    binding.ibCheck.setImageResource(R.drawable.ic_branch_unchecked)
                }
            }
        }
    }
}
