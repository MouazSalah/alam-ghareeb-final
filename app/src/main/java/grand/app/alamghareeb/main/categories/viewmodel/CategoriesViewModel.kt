package grand.app.alamghareeb.main.categories.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.adapter.view.CategoriesAdapter
import grand.app.alamghareeb.main.adapter.view.SlidersAdapter
import grand.app.alamghareeb.main.categories.model.CategoriesResponse
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class CategoriesViewModel @Inject constructor() : BaseViewModel() {
    var categoriesAdapter = CategoriesAdapter()
    var slidersAdapter = SlidersAdapter()

    fun onSearchClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.SEARCH_CLICKED)
    }

    fun getCategories() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.CATEGORIES_DETAILS) }) { res ->
            obsIsProgress.set(false)
            val response: CategoriesResponse = Gson().fromJson(res, CategoriesResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    categoriesAdapter.updateList(response.categoriesData!!.categoriesList as ArrayList<CategoriesItem>)
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    init {
        getCategories()
    }
}