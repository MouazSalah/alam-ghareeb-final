package grand.app.alamghareeb.main.products.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.adapter.view.VerticalProductsAdapter
import grand.app.alamghareeb.main.favorites.model.AddToFavRequest
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.filter.model.FilterRequest
import grand.app.alamghareeb.main.home.model.CategoriesItem
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.products.model.*
import grand.app.alamghareeb.main.products.view.ProductsCategoriesAdapter
import grand.app.alamghareeb.main.products.view.ProductsSubCatAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import timber.log.Timber
import java.util.ArrayList
import javax.inject.Inject

open class ProductsViewModel @Inject constructor() : BaseViewModel() {
    var obsSearchNam = ObservableField<String>()
    var categoriesAdapter = ProductsCategoriesAdapter()
    var subCategoriesAdapter = ProductsSubCatAdapter()
    var productsAdapter = VerticalProductsAdapter()
    var categoryId: Int = -1

    fun onSearchClicked() {
        when {
            obsSearchNam.get() == null || obsSearchNam.get() == "" -> {
                apiResponseLiveData.value =
                    ApiResponse.errorMessage(getString(R.string.msg_empty_search_name))
            }
            else -> {
                apiResponseLiveData.value = ApiResponse.success(Codes.SEARCH_CLICKED)
            }
        }
    }

    fun getProductsByCategory(categoryId: Int) {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({
            getApiRepo().getProductsByCategoryId(URLS.PRODUCTS,
                categoryId)
        }) { res ->
            val response: ProductsResponse = Gson().fromJson(res, ProductsResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    categoriesAdapter.updateList(response.productsCategoryData!!.categoriesList as ArrayList<CategoriesItem>, categoryId)
                    subCategoriesAdapter.updateList(response.productsCategoryData.productCategoryData!!.subCategoriesList as ArrayList<SubCategoriesItem>)
                    productsAdapter.updateList(response.productsCategoryData.productCategoryData.productsData!!.productsList as ArrayList<ProductItem>)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun getProductsBySubCategory(categoryId: Int, subCategoryId: Int) {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().getProductsBySubCategoryId(URLS.PRODUCTS,
                categoryId,
                subCategoryId)
        }) { res ->
            obsIsProgress.set(false)
            val response: SubCategoriesResponse =
                Gson().fromJson(res, SubCategoriesResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    productsAdapter.updateList(response.subCategoriesData?.subCategoryList?.productsList?.productsList as ArrayList<ProductItem>)
                    productsAdapter.notifyDataSetChanged()
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun getAllProducts(categoryId: Int) {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().getProductsByCategoryId(URLS.PRODUCTS,
                categoryId)
        }) { res ->
            obsIsProgress.set(false)
            val response: ProductsResponse =
                Gson().fromJson(res, ProductsResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    productsAdapter.updateList(response.productsCategoryData?.productCategoryData?.productsData!!.productsList as ArrayList<ProductItem>)
                    productsAdapter.notifyDataSetChanged()
                    notifyChange()
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun sortOrders(orderBy: String?) {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().filterOrders(URLS.FILTER_PRODUCTS,
                orderBy.toString())
        }) { res ->
            obsIsProgress.set(false)
            val response: FilterProductsResponse =
                Gson().fromJson(res, FilterProductsResponse::class.java)
            when (response.code) {
                200 -> {
                    productsAdapter.updateList(response.filterProductsData!!.productsList as ArrayList<ProductItem>)
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun filterOrders(request: FilterRequest) {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().requestPostBody(URLS.FILTER_PRODUCTS_RESULT,
                request)
        }) { res ->
            obsIsProgress.set(false)
            val response: FilterProductsResponse =
                Gson().fromJson(res, FilterProductsResponse::class.java)
            when (response.code) {
                200 -> {
                    productsAdapter.updateList(response.filterProductsData!!.productsList as ArrayList<ProductItem>)
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun addToFav(item: ProductItem) {
        val request = AddToFavRequest()
        request.product_id = item.id
        obsIsProgress.set(true)
        requestCall<JsonObject?>({ getApiRepo().requestPostBody(URLS.FAVORITES, request) }) { res ->
            obsIsProgress.set(false)
            val response: AddToFavResponse = Gson().fromJson(res, AddToFavResponse::class.java)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onFilterClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.FILTER_CLICKED)
    }

    fun onSortClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.SORT_CLICKED)
    }
}