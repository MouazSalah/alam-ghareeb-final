package grand.app.alamghareeb.main.addresses.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.databinding.RawAddressBinding
import grand.app.alamghareeb.main.addresses.model.ListAddressItem
import grand.app.alamghareeb.main.addresses.viewmodel.ItemAddressViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class AddressAdapter : RecyclerView.Adapter<AddressAdapter.AddressHolder>() {
    var itemsList = ArrayList<ListAddressItem>()
    var removeLiveData = SingleLiveEvent<ListAddressItem>()
    var addressLiveData = SingleLiveEvent<ListAddressItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawAddressBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_address, parent, false)
        return AddressHolder(binding)
    }

    override fun onBindViewHolder(holder: AddressHolder, position: Int) {
        val itemViewModel = ItemAddressViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setFav()

        holder.binding.rawLayout.setOnClickListener {
            addressLiveData.value = itemViewModel.item
        }

        holder.binding.ibDelete.setOnClickListener {
            removeLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ListAddressItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    fun getItem(pos: Int): ListAddressItem {
        return itemsList[pos]
    }

    fun removeItem(item: ListAddressItem) {
        itemsList.remove(item)
        notifyDataSetChanged()
    }

    inner class AddressHolder(val binding: RawAddressBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setFav() {
            when (getItem(adapterPosition).isDefault) {
                1 -> {
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_branch_checked)
                        .into(binding.ibCheck)
                }
                else -> {
                    Glide.with(BaseApp.getInstance).load(R.drawable.ic_branch_unchecked)
                        .into(binding.ibCheck)
                }
            }
        }
    }
}
