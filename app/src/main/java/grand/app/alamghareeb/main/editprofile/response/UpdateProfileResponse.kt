package grand.app.alamghareeb.main.editprofile.response

import com.google.gson.annotations.SerializedName
import grand.app.alamghareeb.auth.login.model.UserData

data class UpdateProfileResponse(

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val userData: UserData? = null,

    @field:SerializedName("status")
    val status: String? = null
)
