package grand.app.alamghareeb.main.contactus.viewmodel

import androidx.core.content.ContextCompat
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.contactus.model.ContactModel
import grand.app.alamghareeb.main.contactus.model.ContactUsItem
import grand.app.alamghareeb.main.contactus.model.ContactUsResponse
import grand.app.alamghareeb.main.contactus.view.ContactAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class ContactUsViewModel @Inject constructor() : BaseViewModel() {
    var adapter = ContactAdapter()
    var list = arrayListOf<ContactModel>()
    var item1 = ContactModel()
    var item2 = ContactModel()
    var item3 = ContactModel()
    var item4 = ContactModel()

    init {
        getLinks()
    }

    fun getLinks() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.CONTACT_US) }) { res ->
            val response: ContactUsResponse = Gson().fromJson(res, ContactUsResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    getContactItems(response.contactUsList)
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    private fun getContactItems(contactUsList: List<ContactUsItem?>?) {

        item1 = ContactModel(contactUsList!![1]?.id,
            getString(R.string.label_instagram),
            ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_instagram),
            contactUsList[1]?.value)

        item2 = ContactModel(contactUsList[0]!!.id,
            getString(R.string.label_facebook),
            ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_facebook),
            contactUsList[0]?.value)

        item3 = ContactModel(contactUsList[3]?.id,
            getString(R.string.label_gmail),
            ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_gmail),
            contactUsList[3]?.value)

        item4 = ContactModel(contactUsList[2]?.id,
            getString(R.string.label_twitter),
            ContextCompat.getDrawable(BaseApp.getInstance, R.drawable.ic_twitter),
            contactUsList[2]?.value)

        list.add(item1)
        list.add(item2)
        list.add(item3)
        list.add(item4)

        adapter.updateList(list)
    }
}