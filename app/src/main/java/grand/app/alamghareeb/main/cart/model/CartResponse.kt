package grand.app.alamghareeb.main.cart.model

import com.google.gson.annotations.SerializedName

data class CartResponse(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val cartsList: List<CartItem?>? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class CartItem(

    @field:SerializedName("image")
    var image: String? = null,

    @field:SerializedName("price_before")
    var priceBefore: Double? = null,

    @field:SerializedName("total")
    var total: Double? = null,

    @field:SerializedName("is_favorite")
    var isFavorite: Boolean? = null,

    @field:SerializedName("quantity")
    var quantity: Int? = null,

    var stockQuantity: Int? = null,

    @field:SerializedName("colorId")
    var colorId: Int? = null,

    @field:SerializedName("sizeId")
    var sizeId: Int? = null,

    @field:SerializedName("color")
    var color: String? = null,

    @field:SerializedName("size")
    var size: String? = null,

    @field:SerializedName("rate")
    var rate: Float? = null,

    @field:SerializedName("price")
    var price: Double? = null,

    @field:SerializedName("name")
    var name: String? = null,

    @field:SerializedName("id")
    var id: Int? = null
)
