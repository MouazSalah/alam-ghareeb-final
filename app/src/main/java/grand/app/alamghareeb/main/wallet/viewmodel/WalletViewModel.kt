package grand.app.alamghareeb.main.wallet.viewmodel

import androidx.databinding.ObservableField
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.main.toprated.model.TopRatedResponse
import grand.app.alamghareeb.main.wallet.model.WalletProductItem
import grand.app.alamghareeb.main.wallet.model.WalletResponse
import grand.app.alamghareeb.main.wallet.view.WalletAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall
import javax.inject.Inject

open class WalletViewModel @Inject constructor() : BaseViewModel() {
    var adapter = WalletAdapter()
    var obsCurrentCredit = ObservableField<String>()
    var obsTotal: Double = 0.0

    fun getWallet() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.WALLET) }) { res ->
            obsIsProgress.set(false)
            val response: WalletResponse = Gson().fromJson(res, WalletResponse::class.java)
            when (response.code) {
                200 -> {
                    obsLayout.set(LoadingStatus.FULL)
                    obsCurrentCredit.set("${response.walletData?.balance} ${getString(R.string.label_currency)}")
                    obsTotal = response.walletData?.totalRestore!!.toDouble()
                    notifyChange()
                    when {
                        response.walletData.walletProductsList?.size != 0 -> {
                            adapter.updateList(response.walletData.walletProductsList as ArrayList<WalletProductItem>)
                        }
                    }
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onLoginClicked() {
        setValue(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                obsIsVisible.set(true)
                getWallet()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}
