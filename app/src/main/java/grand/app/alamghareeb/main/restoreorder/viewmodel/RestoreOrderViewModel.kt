package grand.app.alamghareeb.main.restoreorder.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.R
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.restoreorder.model.ReasonsResponse
import grand.app.alamghareeb.main.restoreorder.model.RestoreOrderResponse
import grand.app.alamghareeb.main.restoreorder.model.RestoreRequest
import grand.app.alamghareeb.main.restoreorder.view.RestoreProductsAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall

class RestoreOrderViewModel : BaseViewModel() {
    var request = RestoreRequest()
    var adapter = RestoreProductsAdapter()
    var orderId: Int = 1
    var reasonsResponse = ReasonsResponse()

    fun getReasons() {
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.RESTORE_REASONS) }) { res ->
            val response: ReasonsResponse = Gson().fromJson(res, ReasonsResponse::class.java)
            when (response.code) {
                200 -> {
                    reasonsResponse = response
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun returnOrder() {
        obsIsProgress.set(true)
        requestCall<JsonObject?>({
            getApiRepo().requestPostBody(URLS.RESTORE_ORDER + orderId + "/restore",
                request)
        }) { res ->
            val response: RestoreOrderResponse =
                Gson().fromJson(res, RestoreOrderResponse::class.java)
            obsIsProgress.set(false)
            when (response.code) {
                200 -> {
                    apiResponseLiveData.value = ApiResponse.success(response)
                }
                else -> {
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onReasonClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.REASON_CLICKED)
    }

    fun onQuantityClicked() {
        when {
            request.product_id == null -> {
                apiResponseLiveData.value =
                    ApiResponse.successMessage(getString(R.string.msg_choose_returned_product))
            }
            else -> {
                apiResponseLiveData.value = ApiResponse.success(Codes.QUANTITY_CLICKED)
            }
        }
    }

    fun onRestoreClicked() {
        when {
            request.product_id == null -> {
                apiResponseLiveData.value =
                    ApiResponse.successMessage(getString(R.string.msg_choose_returned_product))
            }
            request.quantity == null -> {
                apiResponseLiveData.value =
                    ApiResponse.successMessage(getString(R.string.msg_choose_quantity))
            }
            request.reason == null -> {
                apiResponseLiveData.value =
                    ApiResponse.successMessage(getString(R.string.msg_choose_restore_reason))
            }
            else -> {
                returnOrder()
            }
        }
    }

    init {
        getReasons()
    }
}