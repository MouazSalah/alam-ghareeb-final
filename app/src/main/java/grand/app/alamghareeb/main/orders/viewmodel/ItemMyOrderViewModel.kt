package grand.app.alamghareeb.main.orders.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.orders.model.MyOrderItem
import grand.app.alamghareeb.main.orders.model.OrderProductItem
import grand.app.alamghareeb.main.orders.view.MyOrderProductAdapter
import java.util.ArrayList

class ItemMyOrderViewModel(var item: MyOrderItem) : BaseViewModel() {
    var adapter = MyOrderProductAdapter()

    init {
        adapter.updateList(item.productsList as ArrayList<OrderProductItem>)
    }
}