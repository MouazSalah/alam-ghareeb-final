package grand.app.alamghareeb.main.cart.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentCartBinding
import grand.app.alamghareeb.dialogs.login.DialogLoginFragment
import grand.app.alamghareeb.main.cart.adapter.SwipeDeleting
import grand.app.alamghareeb.main.cart.viewmodel.CartViewModel
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class CartFragment : BaseFragment() {
    lateinit var binding: FragmentCartBinding

    @Inject
    lateinit var viewModel: CartViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getCarts()
        }

        viewModel.getCarts()

//        var swipe = object : SwipeDeleting() { override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
//                return super.onMove(recyclerView, viewHolder, target)
//                Timber.e("mou3az_swipeed : moved")
//            }
//
//            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
//                super.onSwiped(viewHolder, direction)
//                when (direction) {
//                    ItemTouchHelper.LEFT, ItemTouchHelper.RIGHT -> {
//                        Timber.e("mou3az_swipeed")
//                        viewModel.adapter.notifyItemSwiped(viewHolder.adapterPosition)
//                    }
//                }
//            }
//        }
//
//        val touchHelper = ItemTouchHelper(swipe)
//        touchHelper.attachToRecyclerView(binding.rvCarts)

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.LOGIN_CLICKED -> {
                    Const.isAskedToLogin = 1
                    requireActivity().startActivity(Intent(requireActivity(),
                        MainActivity::class.java).putExtra(Const.ACCESS_LOGIN, true))
                }
            }
        }

        observe(viewModel.adapter.removeCartLiveData) {
            viewModel.removeItemFromCart(it!!)
        }

        observe(viewModel.adapter.outOfStock) {
            it?.let { it1 -> showToast(it1, 1) }
        }

        observe(viewModel.adapter.increaseLiveData) {
            when {
                it != null -> {
                    viewModel.inCreaseCartCount(it)
                }
            }
        }

        observe(viewModel.adapter.decreaseLiveData) {
            when {
                it != null -> {
                    viewModel.deCreaseCartCount(it)
                }
            }
        }

        observe(viewModel.apiResponseLiveData) {
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is Int -> {
                            when (it.data) {
                                Codes.CONFIRM_ORDER -> {
                                    when (viewModel.deliveryAdapter.selectedPosition) {
                                        1 -> {
                                            // delivery from home
                                            findNavController().navigate(CartFragmentDirections.cartsToConfirmOrder(
                                                -1))
                                        }
                                        else -> {
                                            // delivering from branch
                                            findNavController().navigate(CartFragmentDirections.cartsToConfirmOrder(
                                                viewModel.deliveryAdapter.branchId))
                                        }
                                    }
                                }
                                Codes.NOT_LOGIN -> {
                                    Utils.startDialogActivity(requireActivity(),
                                        DialogLoginFragment::class.java.name,
                                        Codes.DIALOG_LOGIN_REQUEST,
                                        null)
                                }
                                Codes.ITEM_CART_REMOVED -> {
                                    (requireActivity() as MainActivity).updateCartBadge()
                                }
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(),
                                            AuthActivity::class.java).putExtra(Const.ACCESS_LOGIN,
                                            true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when {
            Const.isAskedToLogin == 1 -> {
                viewModel.getUserStatus()
                Const.isAskedToLogin = 0
            }
        }
    }
}