package grand.app.alamghareeb.main.contactus.viewmodel

import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.main.contactus.model.ContactModel

class ItemContactViewModel(var item: ContactModel) : BaseViewModel()
