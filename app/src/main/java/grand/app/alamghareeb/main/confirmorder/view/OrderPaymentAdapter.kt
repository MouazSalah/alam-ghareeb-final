package grand.app.alamghareeb.main.confirmorder.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawPaymentBinding
import grand.app.alamghareeb.main.confirmorder.model.OrderPaymentItem
import grand.app.alamghareeb.main.confirmorder.viewmodel.ItemOrderPaymentViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import kotlin.collections.ArrayList

class OrderPaymentAdapter : RecyclerView.Adapter<OrderPaymentAdapter.PaymentHolder>() {
    var itemsList: ArrayList<OrderPaymentItem> = ArrayList()
    var itemLiveData = SingleLiveEvent<OrderPaymentItem>()
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawPaymentBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_payment, parent, false)
        return PaymentHolder(binding)
    }

    override fun onBindViewHolder(holder: PaymentHolder, position: Int) {
        val itemViewModel = ItemOrderPaymentViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.setSelected()
        holder.binding.rawLayout.setOnClickListener {
            when {
                position != selectedPosition -> {
                    notifyItemChanged(position)
                    notifyItemChanged(selectedPosition)
                    selectedPosition = position
                    itemLiveData.value = itemViewModel.item
                }
                else -> {
                    selectedPosition = -1
                    notifyItemChanged(position)
                    itemLiveData.value = itemViewModel.item
                }
            }
        }
    }

    fun getItem(pos: Int): OrderPaymentItem {
        return itemsList[pos]
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<OrderPaymentItem>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class PaymentHolder(val binding: RawPaymentBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun setSelected() {
            when (selectedPosition) {
                adapterPosition -> {
                    binding.icChecked.setImageResource(R.drawable.ic_branch_checked)
                }
                else -> {
                    binding.icChecked.setImageResource(R.drawable.ic_branch_unchecked)
                }
            }
        }
    }
}
