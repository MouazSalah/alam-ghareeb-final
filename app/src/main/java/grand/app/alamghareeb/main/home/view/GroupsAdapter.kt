package grand.app.alamghareeb.main.home.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawHomeGroupBinding
import grand.app.alamghareeb.databinding.RawProfileBinding
import grand.app.alamghareeb.main.help.view.ItemHelpModel
import grand.app.alamghareeb.main.help.viewmodel.ItemHelpViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import java.util.*

class GroupsAdapter : RecyclerView.Adapter<GroupsAdapter.GroupsHolder>() {
    var itemsList: ArrayList<ItemHelpModel> = ArrayList()
    var itemLiveData = SingleLiveEvent<ItemHelpModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupsHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawHomeGroupBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_home_group, parent, false)
        return GroupsHolder(binding)
    }

    override fun onBindViewHolder(holder: GroupsHolder, position: Int) {
        val itemViewModel = ItemHelpViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ItemHelpModel>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class GroupsHolder(val binding: RawHomeGroupBinding) :
        RecyclerView.ViewHolder(binding.root)
}
