package grand.app.alamghareeb.main.groups.savingoffers.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.activity.main.BaseHomeFragment
import grand.app.alamghareeb.base.BaseApp
import grand.app.alamghareeb.base.di.IApplicationComponent
import grand.app.alamghareeb.databinding.FragmentSavingOffersBinding
import grand.app.alamghareeb.dialogs.login.DialogLoginFragment
import grand.app.alamghareeb.main.favorites.model.FavoritesResponse
import grand.app.alamghareeb.main.favorites.response.AddToFavResponse
import grand.app.alamghareeb.main.groups.base.GroupsFragmentDirections
import grand.app.alamghareeb.main.groups.savingoffers.viewmodel.SavingOffersViewModel
import grand.app.alamghareeb.main.home.model.ProductItem
import grand.app.alamghareeb.network.Status
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe
import timber.log.Timber
import javax.inject.Inject

class SavingOffersFragment : BaseHomeFragment() {

    private lateinit var binding: FragmentSavingOffersBinding

    @Inject
    lateinit var viewModel: SavingOffersViewModel
    var productItem = ProductItem()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_saving_offers, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ((BaseApp.getInstance.applicationComponent) as IApplicationComponent).inject(this)
        binding.viewModel = viewModel

        viewModel.getProducts()

        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getProducts()
        }

        observe(viewModel.adapter.cartLiveData) {
            navController.navigate(GroupsFragmentDirections.groupsToProductDetails(it?.id!!))
        }

        observe(viewModel.adapter.favLiveData) {
            it?.let { item ->
                when {
                    PrefMethods.getUserData() == null -> {
                        Timber.e("mou3az_login : dialog login request")
                        Utils.startDialogActivity(requireActivity(),
                            DialogLoginFragment::class.java.name,
                            Codes.DIALOG_LOGIN_REQUEST,
                            null)
                    }
                    else -> {
                        productItem = item
                        viewModel.addToFav(item)
                    }
                }
            }
        }

        observe(viewModel.apiResponseLiveData) { it ->
            when (it.status) {
                Status.ERROR_MESSAGE -> {
                    showToast(it.message.toString(), 1)
                }
                Status.SUCCESS_MESSAGE -> {
                    showToast(it.message.toString(), 2)
                }
                Status.SUCCESS -> {
                    when (it.data) {
                        is FavoritesResponse -> {
                        }
                        is AddToFavResponse -> {
                            showToast(it.data.message.toString(), 2)
                            it.data.data?.isFavorite?.let { it1 ->
                                viewModel.adapter.notifyItemSelected(productItem,
                                    it1)
                            }
                        }
                    }
                }
                else -> {
                    Timber.e(it.message)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Timber.e("mou3az_login : dialog login request")
                                        requireActivity().startActivity(Intent(requireActivity(),
                                            AuthActivity::class.java))
                                        requireActivity().finishAffinity()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when {
            Const.isAskedToLogin == 1 && PrefMethods.getUserData() != null -> {
                Const.isAskedToLogin = 0
                viewModel.getProducts()
            }
        }
    }
}