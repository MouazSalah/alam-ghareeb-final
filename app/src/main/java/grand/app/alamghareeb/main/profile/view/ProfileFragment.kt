package grand.app.alamghareeb.main.profile.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.firebase.functions.FirebaseFunctionsException
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.auth.AuthActivity
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentProfileBinding
import grand.app.alamghareeb.main.profile.viewmodel.ProfileViewModel
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.observe
import timber.log.Timber

class ProfileFragment : BaseFragment() {
    lateinit var binding: FragmentProfileBinding
    lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)
        binding.viewModel = viewModel

        when {
            PrefMethods.getUserData() != null -> {
                Timber.e("mou3az logged in")
                Glide.with(this).load(PrefMethods.getUserData()!!.image).error(R.drawable.ic_user)
                    .into(binding.imgItem)
            }
            else -> {
                Timber.e("mou3az logged out")
                Glide.with(this).load(R.drawable.ic_user).into(binding.imgItem)
            }
        }

        observe(viewModel.mutableLiveData) {
            if (it == Codes.EDIT_PROFILE) {
                when {
                    PrefMethods.getUserData() != null -> {
                        findNavController().navigate(R.id.profile_to_edit)
                    }
                }
            }
        }

        observe(viewModel.adapter.itemLiveData) {
            it.let {
                when (it?.id) {
                    0 -> {
                        findNavController().navigate(R.id.profile_to_addresses)
                    }
                    1 -> {
                        findNavController().navigate(R.id.profile_to_my_orders)
                    }
                    2 -> {
                        findNavController().navigate(R.id.profile_to_wallet)
                    }
                    3 -> {
                        findNavController().navigate(R.id.profile_to_favorites)
                    }
                    4 -> {
                        findNavController().navigate(R.id.profile_to_help)
                    }
                    5 -> {
                        when {
                            PrefMethods.getUserData() != null -> {
                                PrefMethods.deleteUserData()
                            }
                        }
                        requireActivity().startActivity(Intent(requireActivity(),
                            AuthActivity::class.java))
                        requireActivity().finishAffinity()
                    }
                }
            }
        }
    }
}