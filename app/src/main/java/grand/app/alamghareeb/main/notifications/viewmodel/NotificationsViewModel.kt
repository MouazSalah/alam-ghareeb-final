package grand.app.alamghareeb.main.notifications.viewmodel

import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.base.LoadingStatus
import grand.app.alamghareeb.main.notifications.model.NotificationItem
import grand.app.alamghareeb.main.notifications.model.NotificationsResponse
import grand.app.alamghareeb.main.notifications.view.NotificationsAdapter
import grand.app.alamghareeb.network.ApiResponse
import grand.app.alamghareeb.network.URLS
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.requestCall

class NotificationsViewModel : BaseViewModel() {
    var adapter = NotificationsAdapter()

    fun getNotifications() {
        obsLayout.set(LoadingStatus.SHIMMER)
        requestCall<JsonObject?>({ getApiRepo().requestGet(URLS.NOTIFICATIONS) }) { res ->
            obsIsProgress.set(false)
            val response: NotificationsResponse =
                Gson().fromJson(res, NotificationsResponse::class.java)
            when (response.code) {
                200 -> {
                    when {
                        response.notificationsData?.notificationsList!!.isNotEmpty() -> {
                            adapter.updateList(response.notificationsData.notificationsList as ArrayList<NotificationItem>)
                            obsLayout.set(LoadingStatus.FULL)
                        }
                        else -> {
                            obsLayout.set(LoadingStatus.EMPTY)
                        }
                    }
                }
                else -> {
                    obsLayout.set(LoadingStatus.EMPTY)
                    apiResponseLiveData.value = ApiResponse.errorMessage(response.message)
                }
            }
        }
    }

    fun onLoginClicked() {
        apiResponseLiveData.value = ApiResponse.success(Codes.LOGIN_CLICKED)
    }

    fun getUserStatus() {
        when {
            PrefMethods.getUserData() != null -> {
                getNotifications()
            }
            else -> {
                obsLayout.set(LoadingStatus.NOTLOGIN)
            }
        }
    }

    init {
        getUserStatus()
    }
}