package grand.app.alamghareeb.main.contactus.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.alamghareeb.R
import grand.app.alamghareeb.databinding.RawContactUsBinding
import grand.app.alamghareeb.main.contactus.model.ContactModel
import grand.app.alamghareeb.main.contactus.viewmodel.ItemContactViewModel
import grand.app.alamghareeb.utils.SingleLiveEvent
import java.util.*

class ContactAdapter : RecyclerView.Adapter<ContactAdapter.ContactHolder>() {
    var itemsList: ArrayList<ContactModel> = ArrayList()
    var itemLiveData = SingleLiveEvent<ContactModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
        val context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        val binding: RawContactUsBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.raw_contact_us, parent, false)
        return ContactHolder(binding)
    }

    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        val itemViewModel = ItemContactViewModel(itemsList[position])
        holder.binding.viewModel = itemViewModel

        holder.binding.rawLayout.setOnClickListener {
            itemLiveData.value = itemViewModel.item
        }
    }

    override fun getItemCount(): Int {
        return itemsList.size
    }

    fun updateList(models: ArrayList<ContactModel>) {
        itemsList = models
        notifyDataSetChanged()
    }

    inner class ContactHolder(val binding: RawContactUsBinding) :
        RecyclerView.ViewHolder(binding.root)
}
