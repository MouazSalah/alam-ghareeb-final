package grand.app.alamghareeb.main.rates.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import grand.app.alamghareeb.R
import grand.app.alamghareeb.activity.main.MainActivity
import grand.app.alamghareeb.base.BaseFragment
import grand.app.alamghareeb.databinding.FragmentRatesBinding
import grand.app.alamghareeb.dialogs.addrate.view.DialogAddRateFragment
import grand.app.alamghareeb.dialogs.login.DialogLoginFragment
import grand.app.alamghareeb.main.rates.viewmodel.RatesViewModel
import grand.app.alamghareeb.utils.Utils
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import grand.app.alamghareeb.utils.observe

class RatesFragment : BaseFragment() {
    lateinit var binding: FragmentRatesBinding
    lateinit var viewModel: RatesViewModel
    private val fragmentArgs: RatesFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rates, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(RatesViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.getRates(fragmentArgs.salonId)
        binding.swipeLayout.setOnRefreshListener {
            binding.swipeLayout.isRefreshing = false
            viewModel.getRates(fragmentArgs.salonId)
        }

        observe(viewModel.mutableLiveData) {
            when (it) {
                Codes.RATE_APP_CLICKED -> {
                    val bundle = Bundle()
                    bundle.putInt(Params.PRODUCT_ID, fragmentArgs.salonId)
                    Utils.startDialogActivity(requireActivity(),
                        DialogAddRateFragment::class.java.name,
                        Codes.DIALOG_ADD_RATE,
                        bundle)
                }
                Codes.NOT_LOGIN -> {
                    Utils.startDialogActivity(requireActivity(),
                        DialogLoginFragment::class.java.name,
                        Codes.DIALOG_LOGIN_REQUEST,
                        null)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            Codes.DIALOG_LOGIN_REQUEST -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        Const.isAskedToLogin = 1
                                        requireActivity().startActivity(Intent(requireActivity(),
                                            MainActivity::class.java).putExtra(Const.ACCESS_LOGIN,
                                            true))
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Codes.DIALOG_ADD_RATE -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        viewModel.getRates(fragmentArgs.salonId)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when {
            Const.isAskedToLogin == 1 -> {
                Const.isAskedToLogin = 0
                viewModel.getRates(fragmentArgs.salonId)
            }
        }
    }
}