package grand.app.alamghareeb.location.map

import android.location.Geocoder
import android.location.Location
import androidx.databinding.ObservableField
import grand.app.alamghareeb.base.BaseViewModel
import grand.app.alamghareeb.location.util.MapUtil
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const

class MapViewModel : BaseViewModel()
{
    val obsAddress = ObservableField<String>()
    var latitude =  Const.latitude
    var longitude = Const.longitude

    fun onBackClicked() {
        setValue(Codes.BACK_PRESSED)
    }

    fun onCurrentLocationClicked() {
        setValue(Codes.GETTING_CURRENT_LOCATION)
    }

    fun onSearchClicked() {
        setValue(Codes.SEARCH_LOCATION_CLICKED)
    }

    fun onSaveClicked() {
        setValue(Codes.CONFIRM_CLICKED)
    }

    fun gotLocation(location: Location, geocoder: Geocoder) {
        obsAddress.set(MapUtil.getLocationAddress(geocoder, location.latitude, location.longitude))
        latitude = location.latitude
        longitude = location.longitude
    }
}