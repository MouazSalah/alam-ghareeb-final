package grand.app.alamghareeb.location.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Geocoder
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.libraries.places.widget.Autocomplete
import grand.app.alamghareeb.activity.BaseActivity
import grand.app.alamghareeb.databinding.ActivityMapsBinding
import grand.app.alamghareeb.utils.constants.Codes
import grand.app.alamghareeb.utils.constants.Const
import grand.app.alamghareeb.utils.constants.Params
import timber.log.Timber
import grand.app.alamghareeb.R
import grand.app.alamghareeb.location.dialogpermission.DialogPermissionFragment
import grand.app.alamghareeb.location.util.AddressItem
import grand.app.alamghareeb.location.util.GpsUtils
import grand.app.alamghareeb.location.util.MapUtil
import grand.app.alamghareeb.location.util.PermissionUtil
import grand.app.alamghareeb.utils.PrefMethods
import grand.app.alamghareeb.utils.Utils
import java.util.*

class MapsActivity : BaseActivity(), OnMapReadyCallback, Observer<Any?> {
    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var viewModel: MapViewModel
    private lateinit var geocoder: Geocoder
    private var fusedLocationClient: FusedLocationProviderClient? = null
    private var locationCallback: LocationCallback? = null
    private var hasIntent: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        viewModel = ViewModelProvider(this).get(MapViewModel::class.java)
        binding.viewModel = viewModel

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        geocoder = Geocoder(this, Locale.forLanguageTag(PrefMethods.getLanguage()))

        when {
            intent.hasExtra(Params.BRANCH_NAME) -> {
                when {
                    intent.getStringExtra(Params.BRANCH_NAME) != null -> {
                        hasIntent = true
                        binding.addressLayout.visibility = View.VISIBLE
                        binding.tvSalonName.text = intent.getStringExtra(Params.BRANCH_NAME)
                        viewModel.obsAddress.set(MapUtil.getLocationAddress(getGeoCoder(),
                            intent.getDoubleExtra(Params.BRANCH_LAT, 30.043917),
                            intent.getDoubleExtra(Params.BRANCH_LONG, 31.010492)))
                    }
                }
            }
        }

        viewModel.mutableLiveData.observe(this, this)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        viewModel.mutableLiveData.observe(this, this)
    }

    /* Checking the LOCATION permission state before asking the user runtime permission */
    private fun requestLocationPermission() {
        /* If user selected NEVER ASK AGAIN OR device policy prohibits the app from having that permission */
        when {
            PrefMethods.getIsPermissionDeniedForEver() -> {
                Utils.startDialogActivity(
                    this@MapsActivity,
                    DialogPermissionFragment::class.java.name,
                    Codes.OPEN_SETTING_DIALOG_REQUEST_CODE,
                    null
                )
            }
            /* If user clicked deny once Or this the first time to open the application */
            else -> {
                ActivityCompat.requestPermissions(
                    this@MapsActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    Codes.ACCESS_LOCATION_REQUEST_CODE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode)
        {
            Codes.ACCESS_LOCATION_REQUEST_CODE -> {
                // Permission is granted. Continue the action or workflow
                when {
                    grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                        // Don't call tis method from here, I handle it (ON RESUME) method to not called twice
                        openLocationFromApp()
                    }
                    else -> {
                        when {
                            ActivityCompat.shouldShowRequestPermissionRationale(this@MapsActivity,
                                Manifest.permission.ACCESS_FINE_LOCATION) -> {
                                // User clicked deny only once
                            }
                            else -> {
                                //Never ask again selected, or device policy prohibits the app from having that permission.
                                //So, disable that feature, or fall back to another situation...
                                PrefMethods.saveIsPermissionDeniedForEver(true)
                            }
                        }
                    }
                }
                return
            }
        }
    }

    /* When user clicked Allow to open GPS without going to setting page */
    private fun openLocationFromApp() {
        GpsUtils(this@MapsActivity).turnGPSOn(object : GpsUtils.onGpsListener {
            override fun gpsStatus(isGPSEnable: Boolean) {
                requestLocationUpdates()
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        Timber.e("mou3az_map : " + intent.getDoubleExtra(Params.BRANCH_LAT, 30.043917))
        Timber.e("mou3az_map : " + intent.getDoubleExtra(Params.BRANCH_LONG, 30.043917))

        val myLocation = LatLng(intent.getDoubleExtra(Params.BRANCH_LAT, 30.043917),
            intent.getDoubleExtra(Params.BRANCH_LONG, 31.010492))

        val marker = (getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.custom_marker, null)
        mMap.addMarker(MarkerOptions().position(myLocation).icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this@MapsActivity, marker))))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, Const.zoomLevel))

//        mMap.setOnMapClickListener { point: LatLng ->
//            googleMap.clear()
//            val userLocation = LatLng(point.latitude, point.longitude)
//            viewModel.latitude = userLocation.latitude
//            viewModel.longitude = userLocation.longitude
//         //   val option = MarkerOptions().position(LatLng(userLocation.latitude, userLocation.longitude))
//         //   mMap.addMarker(option)
//          //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(userLocation.latitude, userLocation.longitude), Const.zoomLevel))
//            viewModel.obsAddress.set(MapUtil.getLocationAddress(getGeoCoder(),
//                userLocation.latitude,
//                userLocation.longitude))
//        }
    }

    override fun onChanged(it: Any?) {
        when (it) {
            null -> return
            else -> when (it) {

                /* When user click CURRENT LOCATION button ... Get current user location */
                Codes.GETTING_CURRENT_LOCATION -> {
                    when {
                        isPermissionGranted() -> {
                            openLocationFromApp()
                        }
                        else -> {
                            requestLocationPermission()
                        }
                    }
                }

                /* When User clicked the search toolbar */
                Codes.SEARCH_LOCATION_CLICKED -> {
                    when {
                        isPermissionGranted() -> {
                            MapUtil.openSearchPlaceScreen(this, it as Int)
                        }
                        else -> {
                            requestLocationPermission()
                        }
                    }
                }

                /* When user click confirm button ... Add this address item to Addresses list and finish the activity*/
                Codes.CONFIRM_CLICKED -> {
                    when (viewModel.latitude) {
                        /*
                        * If latitude and longitude is not selected before ... get current location
                        * and if location permission is not granted .. as user to allow it then turn gps on and get current user location
                        */
                        0.0 -> {
                            when {
                                isPermissionGranted() -> {
                                    requestLocationUpdates()
                                }
                                else -> {
                                    requestLocationPermission()
                                }
                            }
                        }
                        /* If location got save it and close activity */
                        else -> {
                            val userLocation = AddressItem()
                            userLocation.run {
                                lat = viewModel.latitude
                                lng = viewModel.longitude
                                address = MapUtil.getLocationAddress(getGeoCoder(),
                                    viewModel.latitude,
                                    viewModel.longitude)
                            }
                            saveLocationAndCloseActivity(userLocation)
                        }
                    }
                }

                /* When user press back button of toolbar >>> Don't save any data, JUST finish the activity*/
                Codes.BACK_PRESSED -> {
                    val intent = Intent()
                    intent.putExtra(Params.DIALOG_CLICK_ACTION, 0)
                    setResult(Codes.GET_LOCATION_FROM_MAP, intent)
                    finish()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            /* Back from PLACES Search dialog */
            Codes.SEARCH_LOCATION_CLICKED -> {
                when (resultCode) {
                    RESULT_OK -> {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        when {
                            place.latLng != null -> {
                                mMap.clear()
                                viewModel.latitude = place.latLng!!.latitude
                                viewModel.longitude = place.latLng!!.longitude
                                viewModel.obsAddress.set(MapUtil.getLocationAddress(getGeoCoder(), place.latLng!!.latitude, place.latLng!!.longitude))
                                mMap.addMarker(MarkerOptions().position(LatLng(place.latLng!!.latitude,
                                    place.latLng!!.longitude)))
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(place.latLng!!.latitude,
                                    place.latLng!!.longitude), Const.zoomLevel))
                            }
                        }
                    }
                }
            }

            /* Back From Setting page */
            Codes.ALLOW_PERMISSION_FROM_SETTING_PAGE -> {
                when {
                    PermissionUtil.isGranted(this@MapsActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION) -> {
                        PrefMethods.saveIsPermissionDeniedForEver(false)
                        when {
                            isGpsEnabled() -> {
                                requestLocationUpdates()
                            }
                            else -> {
                                openLocationFromApp()
                            }
                        }
                    }
                }
            }

            /* When user clicked confirm to open setting page and allow permission from there */
            Codes.OPEN_SETTING_DIALOG_REQUEST_CODE -> {
                when {
                    data != null -> {
                        when {
                            data.hasExtra(Params.DIALOG_CLICK_ACTION) -> {
                                when {
                                    data.getIntExtra(Params.DIALOG_CLICK_ACTION, 1) == 1 -> {
                                        openAppDetails()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    fun requestLocationUpdates() {
        viewModel.isShown.set(true)
        val locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (10 * 1000).toLong()
        locationRequest.fastestInterval = (2 * 1000).toLong()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                if (locationResult == null) {
                    Timber.e("couldn't get location update")
                } else {
                    Timber.e("$locationResult")
                    if (locationResult.locations.size > 0) {
                        val location = locationResult.locations[0]

                        mMap.clear()
                        viewModel.gotLocation(location!!, getGeoCoder())
                        viewModel.latitude = location.latitude
                        viewModel.longitude = location.longitude
                        viewModel.obsAddress.set(MapUtil.getLocationAddress(getGeoCoder(), location.latitude, location.longitude))

                        val marker = (getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater)
                            .inflate(R.layout.custom_marker, null)

                      //  mMap.addMarker(MarkerOptions().position(LatLng(location.latitude, location.longitude)).icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this@MapsActivity, marker))))

                       // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude), Const.zoomLevel))

                        stopLocationUpdates()
                    }
                }
                viewModel.isShown.set(false)
            }
        }
        try {
            fusedLocationClient?.requestLocationUpdates(locationRequest,
                locationCallback,
                Looper.getMainLooper())
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    fun stopLocationUpdates() {
        locationCallback?.let {
            fusedLocationClient?.removeLocationUpdates(it)
        }
    }

    fun getGeoCoder(): Geocoder {
        return geocoder
    }


    private fun createDrawableFromView(context: Context, view: View): Bitmap?
    {
        val displayMetrics = DisplayMetrics()
        (context as AppCompatActivity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        view.layoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap = Bitmap.createBitmap(
            view.measuredWidth,
            view.measuredHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }
    /* Open app setting details page If user selected deny and don't ask again For the ACCESS_FINE_LOCATION permission */
    private fun openAppDetails() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivityForResult(intent, Codes.ALLOW_PERMISSION_FROM_SETTING_PAGE)
    }

    private fun isPermissionGranted(): Boolean {
        return when {
            !PermissionUtil.isGranted(this@MapsActivity,
                Manifest.permission.ACCESS_FINE_LOCATION) -> {
                false
            }
            else -> {
                true
            }
        }
    }

    private fun isGpsEnabled(): Boolean {
        val locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return when {
            locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) -> {
                true
            }
            else -> {
                false
            }
        }
    }

    private fun saveLocationAndCloseActivity(userLocation: AddressItem) {
        Timber.e("mou3aaz_back save location")
        PrefMethods.saveUserLocation(userLocation)
        val intent = Intent()
        intent.putExtra(Params.DIALOG_CLICK_ACTION, 1)
        intent.putExtra(Params.ADDRESS_ITEM, userLocation)
        setResult(Codes.GET_LOCATION_FROM_MAP, intent)
        finish()
        Timber.e("mou3aaz_back save location result")
    }

    override fun onBackPressed() {
        Timber.e("mou3aaz_back back from map")
        super.onBackPressed()
        val userLocation = AddressItem(viewModel.latitude,
            viewModel.longitude,
            MapUtil.getLocationAddress(geocoder, viewModel.latitude, viewModel.longitude))
        saveLocationAndCloseActivity(userLocation)
        Timber.e("mou3aaz_back back from maps activity")
    }
}